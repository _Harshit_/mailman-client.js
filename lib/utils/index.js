'use strict';
/**
* @module mailman-client
*/

var Promise = require('bluebird');

var _utils = {
    /**
    * No operation function
    * 
    * @method noop
    */
    noop : function() {},

    /**
    * Return a promise to the data with maintaining support for a node style callback(if provided)
    * 
    * @method promisify
    * @param {Object} data The object to which promise is to be provided
    * @return {Promise} A promise to the data
    */
    promisify : function (data,callback) {
        callback = callback || this.noop;
        return new Promise(function (resolve,reject) {
            // the data is already there
            resolve(data);
        }).nodeify(callback);
    },

    /**
    * The plain BlueBird promise with no extra sugar
    * 
    * @method Promise
    * @constructor
    */
    Promise : Promise
};

module.exports = _utils;
