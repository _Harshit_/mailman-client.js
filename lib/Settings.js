'use strict';
/**
* @module mailman-client
* @submodule Settings
*/

var ApiRequest = require('./shared/ApiRequest');
var utils = require('./utils');

var LIST_READ_ONLY_ATTRS = [
    'bounces_address',
    'created_at',
    'digest_last_sent_at',
    'fqdn_listname',
    'http_etag',
    'join_address',
    'last_post_at',
    'leave_address',
    'list_id',
    'list_name',
    'mail_host',
    'next_digest_number',
    'no_reply_address',
    'owner_address',
    'post_id',
    'posting_address',
    'request_address',
    'scheme',
    'volume',
    'web_host',
    ];


/**
* Settings extends ApiRequest to do operations on the settings resource
*
* @constructor
* @class Settings
* @extends ApiRequest
* @param {Object} options A hash of options for the Settings instance
* @param {String} options.endpoint The endpoint URI to request
* @param {String} [options.username] A username for authenticating API requests
* @param {String} [options.password] A password for authenticating API requests
* @param {String} template The URI template used to generate the URI to request 
*/
function Settings(options,template) {
    /**
    * Configuration options such as the endpoint of the API, auth parameters etc.
    * 
    * @private
    * @type Object
    * @property _options
    * @default {}
    */
    this._options = options || {};

    /**
    * Supported HTTP methods
    *
    * @property _supportedMethods
    * @type Array
    * @private
    * @default ['get','patch']
    */
    this._supportedMethods = ['get','patch'];

    /**
    * The URI template used to generate the URI to request 
    *
    * @private
    * @type String
    * @property _template
    */
    this._template = template;

    /**
    * Object containing the resource information, will be null if getInfo() is never called
    *
    * @private
    * @type {Object}
    * @default null
    * @property _info
    */
    this._info = null;

    /**
    * Keys of the _info object
    *
    * @private
    * @type Array
    * @property _keys
    * @default []
    */
    this._keys = [];
    
    /**
    * Length of the Settings object
    *
    * @property length
    * @default 0
    */
    this.length = 0;
}


Settings.prototype = new ApiRequest();


/**
* Fetch the resource by making a HTTP GET request and calls the callback(if any), and provide a 
* promise to the Settings object itself
* 
* @method getInfo
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Settings object itself
* @return {Promise} Promise to the Settings object itself
*/
Settings.prototype.getInfo = function(callback) {
    if ( !this._info ) {
        var that = this;
        return that.get().then(function(data) {
                        that._info = data;
                        return that.getKeys();
                    }).then(function() {
                        that._setAccessors();
                        return utils.promisify(that,callback);
                    }).catch(function(e) {
                        console.error(e);
                    });
    }
    else {
        return utils.promisify(this,callback);
    }
};


/**
* Collect the keys of Settings object in an array if _info is present
*
* @async
* @method keys
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result An array of settings fields
* @return {Promise} Promise to the array of settings fields
*/
Settings.prototype.getKeys = function(callback) {
    var that = this;
    return that.getInfo().then(function() {
                    that._keys = Object.keys(that._info);
                    that.length = that._keys.length;
                    return utils.promisify(that._keys,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Set the accessors on the object for the resource information i.e _info object
*
* @method _setAccessors
* @private
*/
Settings.prototype._setAccessors = function() {

    this._keys.forEach( (function(key) {

        Object.defineProperty(this,key,{

            get : function() {
                if ( key in this._info )
                    return this._info[key];
                else {
                    throw new Error(key + ' not found in the Settings hashmap');
                }
            },

            set : function(value) {
                this._info[key] = value;
            },

            configurable : true
        });

    }).bind(this)

    );
};


/**
* Get the settings value corresponding by providing it's name and calls the callback(if any) 
* with it or provide a promise to it
*
* @async
* @method getValue
* @param {String} key The required setting
* @param {String|null} defaultValue The default value to be set/returned if the required setting is not found
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The required setting
* @return {Promise} Promise to the required setting
*/
Settings.prototype.getValue = function(key,defaultValue,callback) {
    defaultValue = defaultValue || null;
    var that = this;
    return that.getInfo().then(function (data){
                    var retValue;
                    if ( key in that._info ) {
                        retValue = that._info[key];
                    } else {
                        retValue = defaultValue;
                    }
                    return utils.promisify(retValue,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Save the current settings upstream
*
* @async
* @method save
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Settings object itself
* @return {Promise} Promise to the Settings object itself
*/
Settings.prototype.save = function(callback) {
    var data = {};
    var that = this;

    that._keys.forEach(function(key) {

            if ( LIST_READ_ONLY_ATTRS.indexOf(key) === -1) {
                data[key] = that._info[key];
            }
            
        }
    );

    return that.patch(data).then(function() {
                    that._info = null;
                    return utils.promisify(that,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* String representation of the Settings object
*
* @method toString
* @return {String} The string representation of the object
*/
Settings.prototype.toString = function() {
    return JSON.stringify(this._info);
};


module.exports = Settings;