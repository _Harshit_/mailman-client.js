'use strict';
/**
* @module mailman-client
* @submodule Member
*/

var ApiRequest = require('./shared/ApiRequest.js');
var Preferences = require('./Preferences.js');
var User = require('./User.js');
var utils = require('./utils');


/**
* Member extends ApiRequest to perform operations on the member resource
*
* @constructor
* @class Member
* @extends ApiRequest
* @param {Object} options A hash of options for the Member instance
* @param {String} options.endpoint The endpoint URI to request
* @param {String} [options.username] A username for authenticating API requests
* @param {String} [options.password] A password for authenticating API requests
* @param {String} url Location of the member resource on the server
*/
function Member(options,url) {
    /**
    * Configuration options such as the endpoint of the API, auth parameters etc.
    * 
    * @private
    * @type Object
    * @property _options
    * @default {}
    */
    this._options = options || {};

    /**
    * URL of the member resource 
    *
    * @private
    * @type String
    * @property _url
    */
    this._url = url;

    /**
    * Supported HTTP methods
    *
    * @property _supportedMethods
    * @type Array
    * @private
    * @default ['get','delete']
    */
    this._supportedMethods = ['get','delete'];

    /** 
    * Root template string for the member resource used to compose the _template value
    * 
    * @property _rootTemplate
    * @type String
    * @private
    */
    this._rootTemplate = this._url.split(this._options.endpoint)[1];

    /**
    * The URI template used to generate the URI to request 
    *
    * @private
    * @type String
    * @default ''
    * @property _template
    */
    this._template = '';

    /**
    * Object containing the resource information, will be null if getInfo() is never called
    *
    * @private
    * @type {Object}
    * @default null
    * @property _info
    */
    this._info = null;

    /**
    * The Preferences instance corresponding to this Member instance
    * 
    * @private
    * @type Preferences
    * @property _preferences
    * @default null
    */
    this._preferences = null;

    /**
    * Array of properties based on the Member _info object
    *
    * @private
    * @type Array
    * @property _keys
    */
    this._keys = ['email','role','list_id','self_link'];
}


Member.prototype  = new ApiRequest();

/**
* Fetch the resource by making a HTTP GET request and calls the callback(if any), and provide a 
* promise to the Member object itself
* 
* @method getInfo
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Member object itself
* @return {Promise} Promise to the Member object itself
*/
Member.prototype.getInfo = function(callback) {
    this._template = this._rootTemplate;
    if ( !this._info) {
        var that = this;
        return that.get().then(function(data) {
                        that._info = data;
                        that._setAccessors();
                        return utils.promisify(that,callback);
                    }).catch(function(e) {
                        console.error(e);
                    });
    } else {
        return utils.promisify(this,callback);
    }
};


/**
* Set the accessors on the object for the resource information i.e _info object
*
* @method _setAccessors
* @private
*/
Member.prototype._setAccessors = function() {
    /**
    * List ID of the list to which the Member instance is subscribed
    *
    * @property list_id
    * @type String
    */

    /**
    * Email address of the member
    *
    * @property email
    * @type String
    */

    /**
    * Associated role of the member
    *
    * @property member
    * @type String
    */

    /**
    * URI of the member resource
    *
    * @property self_link
    * @type String
    */
    this._keys.forEach( (function(key) {
        Object.defineProperty(this,key,{
                get : function() {
                    if ( key in this._info )
                        return this._info[key];
                    else 
                        throw new Error('KeyError : key not found in the info hashmap');
                },

                configurable : true
            });
        }).bind(this)
    );

    /**
    * Getter for the Preferences instance corresponding to the current Member instance
    * 
    * @property preferences
    * @type Preferences
    */
    Object.defineProperty(this,'preferences',{
        get : function() {
            if ( !this._preferences )
            {   
                var endpoint = this._rootTemplate + '/preferences';
                this._preferences = new Preferences(this._options,endpoint);
            }
            return this._preferences;
        }
    });

    /**
    * Getter for the User instance corresponding to the current Member instance
    * 
    * @property user
    * @type User
    */
    Object.defineProperty(this,'user',{
        get : function() {
            return new User(this._options,this._info.user);
        }
    });
};


/** 
* Unsubscribe a Member from a mailing list
* 
* @method unsubscribe
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Member object itself
* @return {Promise} Promise to the Member object itself
*/
Member.prototype.unsubscribe = function(callback) {
    var that = this;
    that._template = that._rootTemplate;
    return that.delete().then(function(data) {
                    return utils.promisify(that,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* String representation of the Member object
*
* @method toString
* @return {String} Returns the string representation of the Member object
*/
Member.prototype.toString = function() {
    return '<Member ' + this.email + ' on ' + this.list_id + ' >';
};


module.exports = Member;