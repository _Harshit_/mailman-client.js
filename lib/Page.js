'use strict';
/**
* @module mailman-client
* @submodule Page
*/

var ApiRequest = require('./shared/ApiRequest.js');
var utils = require('./utils');

var DEFAULT_PAGE_ITEM_COUNT = 50;

/**
* Page extends ApiRequest to provide pagination functionality 
* 
* @constructor
* @class Page
* @extends ApiRequest
* @param {Object} options A hash of options for the Member instance
* @param {String} options.endpoint The endpoint URI to request
* @param {String} [options.username] A username for authenticating API requests
* @param {String} [options.password] A password for authenticating API requests
* @param {String} path The path to be requested
* @param {Object} model Resource class on which pagination is to be done, eg. List, Domain etc.
* @param {Number} count Number of items on a page
* @param {Number} page Number of pages to be requested 
*/
function Page(options,path,model,count,page) {
    /**
    * Configuration options such as the endpoint of the API, auth parameters etc.
    * 
    * @private
    * @type Object
    * @property _options
    * @default {}
    */
    this._options = options || {};

     /**
    * The URI template used to generate the URI to request 
    *
    * @private
    * @type String
    * @property _template
    */
    this._template = path;

    /**
    * Supported HTTP methods
    *
    * @property _supportedMethods
    * @type Array
    * @private
    * @default ['get']
    */
    this._supportedMethods = ['get'];

    /**
    * Supported HTTP methods
    *
    * @property _supportedMethods
    * @type Array
    * @private
    * @default ['get','delete']
    */
    this._model = model;

    this._count = count;

    this._page = page;

    this._entries = null;
}


Page.prototype = new ApiRequest();


/**
* Creates the page corresponding to the Model class
*
* @async
* @private
* @method _createPage
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Page object itself
* @return {Promise} Promise to the Page object itself
*/
Page.prototype._createPage = function(callback) {
    if (!this._entries) {
        var that = this;
        that._entries = [];
        return that.query({count : that._count.toString(), page : that._page.toString()})
                   .get()
                   .then(function(resp) {
                        if ( 'entries' in resp ) {
                            resp.entries.forEach(function(entry) {
                                that._entries.push(new that._model(that._options,entry.self_link));
                            });
                        }
                        that.length = that._entries.length;
                        return utils.promisify(that,callback);
                   });
    } else {
        return utils.promisify(this,callback);
    }
};


/**
* Get the current page
* 
* @async
* @method current
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Page object itself
* @return {Promise} Promise to the Page object itself
*/
Page.prototype.current = function(callback) {
    return this._createPage(callback);
};


/**
* Get the next page
* 
* @async
* @method next
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Page object itself
* @return {Promise} Promise to the Page object itself
*/
Page.prototype.next = function(callback) {
    this._page++;
    this._entries = null;
    return this._createPage(callback);
};


/**
* Get the previous page
* 
* @async
* @method previous
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Page object itself
* @return {Promise} Promise to the Page object itself
*/
Page.prototype.previous = function(callback) {
    if  ( this._page > 1 ) {
        this._page--;
        this._entries = null;
        return this._createPage(callback);
    }
};


/**
* Iterate over all the entries in the current page
* 
* @async
* @method each
* @param {Function} callback The callback function to be invoked with each page entry
*/
Page.prototype.each = function(callback) {
    var that = this;
    that.current().then(function() {
        that._entries.forEach(function(entry) {
            callback(entry);
        });
    });
};


/**
* Calls the callback with all the entries of the current page
*
* @async
* @method all
* @param {Function} callback The callback function to be invoked
*/
Page.prototype.all = function(callback) {
    var that = this;
    return that.current().then(function() {
        return utils.promisify(that._entries,callback);
    });
};


/**
* String representation of the Page object
*
* @method toString
* @return {String} Returns the string representation of the Page object
*/
Page.prototype.toString = function() {
    return '<Page ' + this._page + ' (' + this._model + ')>';
};


module.exports = Page;