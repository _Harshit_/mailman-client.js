'use strict';
/**
* @module mailman-client
* @submodule Domain
*/

var ApiRequest = require('./shared/ApiRequest.js');
var List = require('./List.js');
var User = require('./User.js');
var utils = require('./utils');


/**
* Domain extends ApiRequest to perform operations on the domain resource
*
* @class Domain
* @constructor
* @extends ApiRequest
* @param {Object} options A hash of options for the Domain instance
* @param {String} options.endpoint The endpoint URI to request
* @param {String} [options.username] A username for authenticating API requests
* @param {String} [options.password] A password for authenticating API requests
* @param {String} url Location of the domain resource on the server
*/
function Domain(options,url) {
    /**
    * Configuration options such as the endpoint of the API, auth parameters etc.
    * 
    * @private
    * @type Object
    * @property _options
    * @default {}
    */
    this._options = options || {};

    /**
    * URL of the domain resource 
    *
    * @private
    * @type String
    * @property _url
    */
    this._url = url;

    /** 
    * Root template string for the domain resource used to assemble other endpoints under this category
    * 
    * @property _rootTemplate
    * @type String
    * @private
    */
    this._rootTemplate = this._url.split(this._options.endpoint)[1];

    /**
    * The URI template used to generate the URI to request 
    *
    * @private
    * @type String
    * @default ''
    * @property _template
    */
    this._template = '';

    /**
    * Supported HTTP methods
    *
    * @property _supportedMethods
    * @type Array
    * @private
    * @default ['get','post','delete']
    */
    this._supportedMethods = ['get','post','delete'];

    /**
    * Object containing the resource information, will be null if getInfo() is never called
    *
    * @private
    * @type {Object}
    * @default null
    * @property _info
    */
    this._info = null;

    /**
    * Keys of the _info object
    *
    * @private
    * @type Array
    * @property _keys
    * @default []
    */
    this._keys = [];

    /**
    * Object containing request path variables used to assemble the request URI
    *
    * @property _path
    * @type Object
    * @private
    * @default {}
    */
    this._path = {};
}


Domain.prototype = new ApiRequest();


/**
* Fetch the resource by making a HTTP GET request and calls the callback(if any), and provide a 
* promise to the Domain object itself
* 
* @method getInfo
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Domain object itself
* @return {Promise} Promise to the Domain object itself
*/
Domain.prototype.getInfo = function(callback) {
    if (!this._info) {
        var that = this;
        that._template = that._rootTemplate;
        return that.get().then(function(resp) {
                        that._info = resp;
                        that._keys = Object.keys(resp);
                        that._setAccessors();
                        return utils.promisify(that,callback);
                    }).catch(function(e) {
                        console.error(e);
                    });
    }
    else {
        return utils.promisify(this,callback);
    }
};


/**
* Set the accessors on the object for the resource information i.e _info object
*
* @method _setAccessors
* @private
*/
Domain.prototype._setAccessors = function() {
    /**
    * Base URL of the domain
    *
    * @property base_url
    * @type String
    */

    /**
    * Description of the domain
    *
    * @property description
    * @type String
    */

    /**
    * Mail host of the domain
    * @property mail_host
    * @type String
    */

    /** 
    * URL host of the domain
    *
    * @property url_host
    * @type String
    */
    var that = this;
    that._keys.forEach(function(key) {
        Object.defineProperty(that,key, {
            get : function() {
                if ( key in that._info )
                    return that._info[key];
            },
            configurable : true
        });
    });
};


/**
* Gives the owners i.e User instances corresponding to the domain
*
* @async
* @method getOwners
* @param {Function} [callback] The callback function to be invoked with an array of User instances
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result An array of User instances
* @return {Promise} Promise to an array of User instances
*/
Domain.prototype.getOwners = function(callback) {
    var that = this;
    that._template = that._rootTemplate + '/owners';
    return that.get().then(function(resp) {
                    var items = [];
                    if ( 'entries' in resp ) {
                        resp.entries.forEach(function(entry) {
                            items.push(new User(that._options,entry.self_link));
                        });
                    }
                    return utils.promisify(items,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Gives the lists i.e List instances under the current domain
* 
* @async
* @method getLists
* @param {Function} [callback] The callback function to be invoked with an array of List instances
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result An array of List instances
* @return {Promise} Promise to an array of List instances
*/
Domain.prototype.getLists = function(callback) {
    var that = this;
    return that.getInfo().then(function() {
                    that._template = '/domains/(:mailHost)/lists';
                    that._path.mailHost = that.mail_host;
                    return that.get();
               }).then(function(resp) {
                    var items = [];
                    if ( 'entries' in  resp ) {
                        resp.entries.forEach(function(entry) {
                            items.push(new List(that._options,entry['self_link']));
                        });
                    }
                    items.sort(function(x,y) {
                        return (x._rootTemplate > y._rootTemplate) - (x._rootTemplate < y._rootTemplate);
                    });
                    return utils.promisify(items,callback);
               }).catch(function(e) {
                    console.error(e);
               });
};
 

/**
* Create a List correspoding to the domain
* 
* @async
* @method createList
* @param {String} listName Name of the list to be ctreated
* @param {Function} [callback] The callback function to be invoked with the created List instance
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The created List instance
* @return {Promise} Promise to the created List instance
*/
Domain.prototype.createList = function(listName,callback) {
    var that = this;
    return  that.getInfo().then(function() {
                    var payload = {
                        fqdn_listname : listName + '@' + that.mail_host
                    };
                    that._template = '/lists';
                    // get the raw response object
                    return that.post(payload,null,'identity');
                }).then(function(resp) {
                    return utils.promisify(new List(that._options,resp.headers.location),callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Remove all the list owners
*
* @method removeOwners
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Domain object itself
* @return {Promise} Promise to the Domain object itself
*/
Domain.prototype.removeOwners = function(callback) {
    var that = this;
    that._template = that._rootTemplate + '/owners';
    return that.delete().then(function() {
                    return utils.promisify(that,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Add a list owner
*
* @async
* @method addOwner
* @param {String} owner Email address of the owner to be added 
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Domain object itself
* @return {Promise} Promise to the Domain object itself
*/
Domain.prototype.addOwner = function(owner,callback) {
    var that = this;
    that._template = that._rootTemplate + '/owners';
    var payload = {'owner' : owner};
    return that.post(payload).then(function() {
                    return utils.promisify(that,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* String representation of the Domain object
*
* @method toString
* @return {String} Returns the string representation of the Domain object
*/
Domain.prototype.toString = function() {
    return '<Domain ' + this._url.split(this._options.endpoint +'/domains/')[1] + '>';
};


module.exports = Domain;
