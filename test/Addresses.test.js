'use strict';

/**
* Unit tests for the Addresses module
**/

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
chai.use(require('sinon-chai'));
var nock = require('nock');
var path = require('path');

var ApiRequest = require('../lib/shared/ApiRequest.js');
var BaseRequest = require('../lib/shared/BaseRequest.js');
var Addresses = require('../lib/Addresses.js');
var Address = require('../lib/Address.js');

// loading fixtures
var fixturesPath = path.join(__dirname,'fixtures/addresses.test.fixtures.js');
try {
    require(fixturesPath);
} catch(e) {
    throw new Error('Fixtures file not found');
}


describe('Addresses',function() {
    var addrsRequest; // Addresses instance

    var responseData = {    
                            http_etag: '"b896348aea297fa222ea2a7582d47a62612310d8"',
                            entries: [ 
                            {  original_email: 'johndoe@yahoo.com',
                               http_etag: '"6a576675fe4363c353bec0ad2bb44ee5591e1111"',
                               self_link: 'http://localhost:8001/3.0/addresses/johndoe@yahoo.com',
                               email: 'johndoe@yahoo.com',
                               user: 'http://localhost:8001/3.0/users/97071403524618786289886643396072006536',
                               registered_on: '2015-06-18T06:55:32.191706' },
                            {  original_email: 'johndoe@mailman.com',
                               http_etag: '"e37d8c4c160be1f1f5e57e9f6f05e7984235949c"',
                               self_link: 'http://localhost:8001/3.0/addresses/johndoe@mailman.com',
                               email: 'johndoe@mailman.com',
                               user: 'http://localhost:8001/3.0/users/97071403524618786289886643396072006536',
                               registered_on: '2015-06-18T06:42:21.623055' },
                            {  original_email: 'johndoe@gmail.com',
                               http_etag: '"bbb6030c8316118bcc5c84e5cd79a2d55af4dc83"',
                               self_link: 'http://localhost:8001/3.0/addresses/johndoe@gmail.com',
                               email: 'johndoe@gmail.com',
                               verified_on: '2015-06-18T18:10:07.899016',
                               display_name: 'None',
                               user: 'http://localhost:8001/3.0/users/97071403524618786289886643396072006536',
                               registered_on: '2015-06-17T10:16:24.592951' } 
                            ],
                            start: 0,
                            total_size: 3 
                        };

    var addressItems = []; // array of Address objects corresponding to the responseData
    responseData.entries.forEach(function(addrEntry) {
        addressItems.push(new Address({ endpoint:'http://localhost:8001/3.0',
                                username : 'restadmin',
                                password : 'restpass',
                                forceAuth : true },
                                addrEntry
                                ));
    });

    beforeEach(function() {
        addrsRequest = new Addresses({ endpoint:'http://localhost:8001/3.0',
                                    username : 'restadmin',
                                    password : 'restpass' },
                                    '/users/(:user)/addresses',
                                    '97071403524618786289886643396072006536'
                                    );
    });

    describe('Constructor',function() {

        it('should be an instance of ApiRequest',function() {
            expect(addrsRequest).to.be.instanceof(ApiRequest);
        });

        it('should be an instance of BaseRequest',function() {
            expect(addrsRequest).to.be.instanceof(BaseRequest);
        });

        it('should be an instance of Addresses',function() {
            expect(addrsRequest).to.be.instanceof(Addresses);
        });

        it('should intialise the instance properties',function() {
            expect(addrsRequest._options).to.be.deep.equal({ endpoint:'http://localhost:8001/3.0',
                                                            username : 'restadmin',
                                                            password : 'restpass' });
            expect(addrsRequest._template).to.be.equal('/users/(:user)/addresses');
            expect(addrsRequest._path).to.be.deep.equal({ user : '97071403524618786289886643396072006536'});
            expect(addrsRequest._userId).to.be.equal('97071403524618786289886643396072006536');
            expect(addrsRequest._supportedMethods).to.be.deep.equal(['get']);
        });

    }); // constructor
    

    describe('.getAddresses()',function() {
        
        it('should call the passed in callback function with the associated Address objects',function(done) {
            var callback = function(err,retValue) {
                // pass
            };
            var callbackSpy = sinon.spy(callback);
            addrsRequest.getAddresses(callbackSpy).then( function() {
                expect(callbackSpy).to.have.been.calledOnce;
                expect(callbackSpy).to.have.been.calledWith(null,addressItems);
                expect(addrsRequest._addresses).to.be.deep.equal(responseData.entries);
                done();
            });
        });
            
        it('should return a promise to the response data',function(done) {
            addrsRequest.getAddresses().then(function(retData) {
               expect(retData).to.be.deep.equal(addressItems);
               expect(addrsRequest._addresses).to.be.deep.equal(responseData.entries);
               done();
            });
        });

    }); // .getAddresses()

    describe('.each()',function(done) {
        var callbackSpy,loopCount = 0;

        after(function() {
            expect(callbackSpy).to.have.callCount(addressItems.length);
        });

        it('should call the callback with Address instances',function(done) {
            function callback(data) {
                expect(data).to.be.deep.equal(addressItems[loopCount]);
                loopCount++;
                if ( loopCount === addressItems.length ) {
                    done();
                }
            }
            callbackSpy = sinon.spy(callback)
            addrsRequest.each(callbackSpy);
        });


    }); // .each()


    describe('.filterByEmail()',function(done) {

        it('should return an Address instance by using email as a filter',function(done) {
            addrsRequest.filterByEmail('johndoe@gmail.com').then(function(addrInstance) {
                expect(addrInstance).to.be.instanceof(Address);
                expect(addrInstance.email).to.be.equal('johndoe@gmail.com');
                done();
            });
        });

        it('should throw an error if email is not supplied',function() {
            expect(addrsRequest.filterByEmail.bind(addrsRequest)).to.throw(Error);
        });

        it('should return null if email is not found',function(done) {
            addrsRequest.filterByEmail('foobar@gmail.com').then(function(addrInstance) {
                expect(addrInstance).to.be.null;
                done();
            });
        });

    }); // .filterByEmail()

}); // Addresses