'use strict';

/* 
Unit tests for the Preferences module
*/


var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
chai.use(require('sinon-chai'));
var nock = require('nock');
var path = require('path');

var ApiRequest = require('../lib/shared/ApiRequest.js');
var BaseRequest = require('../lib/shared/BaseRequest.js');
var Settings = require('../lib/Settings.js');
var Preferences = require('../lib/Preferences.js');


// loading fixtures
var fixturesPath = path.join(__dirname,'fixtures/preferences.test.fixtures.js');
try {
    require(fixturesPath);
} catch(e) {
    console.log(e);
    throw new Error('Fixtures file not found');
}


describe('Preferences',function() {
    var preferencesInstance, options, responseData, PREFERENCE_FIELDS, PREF_READ_ONLY_ATTRS;

    options = {
        endpoint : 'http://localhost:8001/3.0',
        username : 'restadmin',
        password : 'restpass'
    };

    responseData = { 
        acknowledge_posts: false,
        delivery_mode: 'regular',
        delivery_status: 'enabled',
        hide_address: true,
        preferred_language: 'en',
        receive_list_copy: true,
        receive_own_postings: true,
        self_link : "http://localhost:8001/3.0/system/preferences",
        http_etag : "\"e93578021ad619b9f6a8a5ce2f9f00198e5eb53d\""
    };

    PREFERENCE_FIELDS = [
        'acknowledge_posts',
        'delivery_mode',
        'delivery_status',
        'hide_address',
        'preferred_language',
        'receive_list_copy',
        'receive_own_postings',
    ];

    PREF_READ_ONLY_ATTRS = [
        'http_etag',
        'self_link',
    ];

    beforeEach(function() {
        preferencesInstance = new Preferences(options,
                                              '/system/preferences');
    }); 

    describe('constructor',function() {

        it('should be an instance of BaseRequest',function() {
            expect(preferencesInstance).to.be.instanceof(BaseRequest);
        });

        it('should be an instance of ApiRequest',function() {
            expect(preferencesInstance).to.be.instanceof(ApiRequest);
        });

        it('should be an instance of Preferences',function() {
            expect(preferencesInstance).to.be.instanceof(Preferences);
        });

        it('should initialise the instance properties',function() {
            expect(preferencesInstance._supportedMethods).to.be.deep.equal(['get','patch']);
            expect(preferencesInstance._template).to.be.equal('/system/preferences');
        });

    }); // constructor

    describe('.getPreferences()',function(done) {

        it('._renderURI()',function(done) {
            preferencesInstance.getPreferences().then(function() {
                expect(preferencesInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/system/preferences');
                done();
            });
        }); 

        it('should call the passed in callback function with the Preferences instance',function(done) {
            var callback = function(err,data) {
                // pass
            }
            var callbackSpy = sinon.spy(callback);
            preferencesInstance.getPreferences(callbackSpy).then(function() {
                expect(callbackSpy).to.be.calledOnce;
                expect(callbackSpy).to.be.calledWith(null,preferencesInstance);
                for ( var key in preferencesInstance._preferences ) {
                    if ( key in responseData ) 
                        expect(preferencesInstance._preferences[key]).to.be.equal(responseData[key]);
                    else
                        expect(preferencesInstance._preferences[key]).to.be.equal(null);
                }
                done();
            });
        });

        it('should return a promise to the Preferences instance',function(done) {
            preferencesInstance.getPreferences().then(function(retValue) {
                expect(retValue).to.be.deep.equal(preferencesInstance);
                for ( var key in preferencesInstance._preferences ) {
                    if ( key in responseData ) 
                        expect(preferencesInstance._preferences[key]).to.be.equal(responseData[key]);
                    else
                        expect(preferencesInstance._preferences[key]).to.be.equal(null);
                }
                done();
            });
        }); 

    }); // .getPreferences()

    describe('._setAccessors()',function(done) {

        it('should set the accessors on the Preferences instance',function(done) {
            preferencesInstance.getPreferences().then(function() {
                expect(preferencesInstance.acknowledge_posts).to.be.false;
                preferencesInstance.acknowledge_posts = true;
                expect(preferencesInstance.acknowledge_posts).to.be.true;
                expect(preferencesInstance._preferences['acknowledge_posts']).to.be.equal(true);
                done();
            });
        });

    }); // ._setAccessors()

    describe('.keys()',function(done) {

        it('should return promise to the preferences fields',function(done) {
            preferencesInstance.keys().then(function(retValue) {
                expect(retValue).to.be.deep.equal(Object.keys(preferencesInstance._preferences));
                done();
            });
        });

    }); // .keys()

    describe('.save()',function(done) {

        it('should save the preferences upstream',function(done) {
            // override the behavior of the PATCH method
            var patchStub = function() {
                return {
                    then : function(callback) {
                        callback();
                        return this;
                    },
                    catch : function() {}
                };
            }
            var httpMethodSpy = sinon.stub(preferencesInstance,'patch',patchStub);

            preferencesInstance.getPreferences().then(function() {
                return preferencesInstance.save();
            }).then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                var payload = httpMethodSpy.getCall(0).args[0];
                var payloadKeys = Object.keys(payload);
                payloadKeys.forEach(function(key) {
                    expect(PREF_READ_ONLY_ATTRS).not.to.have.any.keys(key);
                    expect(payload[key]).to.be.equal(preferencesInstance._preferences[key]);
                    expect(payload[key]).not.to.be.null;
                });
                done();
            });
        });

    }); // .save()

    describe('.toString()',function(done) {

        it('should have a string representation',function(done) {
            preferencesInstance.getPreferences().then(function() {
                expect(preferencesInstance.toString()).to.be.deep.equal(JSON.stringify(preferencesInstance._preferences));
                done();
            });
        });

    }); // .toString()

}); // Preferences