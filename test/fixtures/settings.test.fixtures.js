var nock = require('nock');

nock('http://localhost:8001')
  .get('/3.0/lists/foo_list@somedomain.org/config')
  .times(8)
  .reply(200, {"autorespond_owner":"none","scheme":"http","admin_notify_mchanges":false,"autoresponse_grace_period":"90d","posting_pipeline":"default-posting-pipeline","administrivia":true,"leave_address":"foo_list-leave@somedomain.org","next_digest_number":1,"autoresponse_request_text":"","digest_last_sent_at":null,"autorespond_postings":"none","post_id":1,"first_strip_reply_to":false,"bounces_address":"foo_list-bounces@somedomain.org","default_member_action":"defer","http_etag":"\"286754187f4dfc4b4c6ac3c316f6a7e4e21fb1c3\"","web_host":"somedomain.org","posting_address":"foo_list@somedomain.org","advertised":true,"reply_to_address":"","acceptable_aliases":[],"subscription_policy":"moderate","welcome_message_uri":"mailman:///welcome.txt","autoresponse_owner_text":"","fqdn_listname":"foo_list@somedomain.org","description":"List 2 for testing purposes","archive_policy":"public","no_reply_address":"noreply@somedomain.org","last_post_at":null,"default_nonmember_action":"hold","anonymous_list":false,"request_address":"foo_list-request@somedomain.org","join_address":"foo_list-join@somedomain.org","collapse_alternatives":true,"list_name":"foo_list","volume":1,"autoresponse_postings_text":"","display_name":"List_2","autorespond_requests":"none","digest_size_threshold":30,"mail_host":"somedomain.org","filter_content":false,"admin_immed_notify":true,"reply_goes_to_list":"no_munging","send_welcome_message":true,"created_at":"2015-06-17T17:06:21.155428","include_rfc2369_headers":true,"subject_prefix":"[List_2] ","allow_list_posts":true,"convert_html_to_plaintext":false,"owner_address":"foo_list-owner@somedomain.org"}, { date: 'Wed, 29 Jul 2015 17:12:11 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '1685',
  'content-type': 'application/json; charset=utf-8' });

nock('http://localhost:8001')
  .patch('/3.0/lists/foo_list@somedomain.org/config', "autorespond_owner=none&admin_notify_mchanges=false&autoresponse_grace_period=90d&posting_pipeline=default-posting-pipeline&administrivia=true&autoresponse_request_text=&autorespond_postings=none&first_strip_reply_to=false&default_member_action=defer&advertised=true&reply_to_address=&&subscription_policy=moderate&welcome_message_uri=mailman%3A%2F%2F%2Fwelcome.txt&autoresponse_owner_text=&description=List%202%20for%20testing%20purposes&archive_policy=public&default_nonmember_action=hold&anonymous_list=false&collapse_alternatives=true&autoresponse_postings_text=&display_name=List_2&autorespond_requests=none&digest_size_threshold=30&filter_content=false&admin_immed_notify=true&reply_goes_to_list=no_munging&send_welcome_message=true&include_rfc2369_headers=true&subject_prefix=%5BList_2%5D%20&allow_list_posts=true&convert_html_to_plaintext=false")
  .reply(204, "", { date: 'Wed, 29 Jul 2015 17:36:41 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });
