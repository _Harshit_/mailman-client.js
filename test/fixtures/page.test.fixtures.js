var nock = require('nock');

nock('http://localhost:8001')
  .get('/3.0/lists?count=3&page=1')
  .times(5)
  .reply(200, {"total_size":3,"entries":[{"display_name":"FooList","self_link":"http://localhost:8001/3.0/lists/foolist.somedomain.org","mail_host":"somedomain.org","volume":1,"fqdn_listname":"foolist@somedomain.org","member_count":0,"list_name":"foolist","list_id":"foolist.somedomain.org","http_etag":"\"87b1483f06458ed08fb31c309a7b57ce72fdae36\""},{"display_name":"FooBarList","self_link":"http://localhost:8001/3.0/lists/foobarlist.somedomain1.org","mail_host":"somedomain1.org","volume":1,"fqdn_listname":"foobarlist@somedomain1.org","member_count":0,"list_name":"foobarlist","list_id":"foobarlist.somedomain1.org","http_etag":"\"ba516f01a494004646cea53cebc1d3091230513b\""},{"display_name":"Dummy_List","self_link":"http://localhost:8001/3.0/lists/dummylist.somedomain.org","mail_host":"somedomain.org","volume":1,"fqdn_listname":"dummylist@somedomain.org","member_count":0,"list_name":"dummylist","list_id":"dummylist.somedomain.org","http_etag":"\"b804d45faa77a7418a2a9e03030cc5a29becd306\""}],"http_etag":"\"f4905cd1401fc3eb9f8cfba5617b075f9b1591df\"","start":0}, { date: 'Fri, 31 Jul 2015 04:47:50 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '1068',
  'content-type': 'application/json; charset=utf-8' });
