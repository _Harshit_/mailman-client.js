var nock = require('nock');

nock('http://localhost:8001')
  .get('/3.0/users/97071403524618786289886643396072006536/addresses')
  .times(5)
  .reply(200, {"http_etag":"\"b896348aea297fa222ea2a7582d47a62612310d8\"","entries":[{"original_email":"johndoe@yahoo.com","http_etag":"\"6a576675fe4363c353bec0ad2bb44ee5591e1111\"","self_link":"http://localhost:8001/3.0/addresses/johndoe@yahoo.com","email":"johndoe@yahoo.com","user":"http://localhost:8001/3.0/users/97071403524618786289886643396072006536","registered_on":"2015-06-18T06:55:32.191706"},{"original_email":"johndoe@mailman.com","http_etag":"\"e37d8c4c160be1f1f5e57e9f6f05e7984235949c\"","self_link":"http://localhost:8001/3.0/addresses/johndoe@mailman.com","email":"johndoe@mailman.com","user":"http://localhost:8001/3.0/users/97071403524618786289886643396072006536","registered_on":"2015-06-18T06:42:21.623055"},{"original_email":"johndoe@gmail.com","http_etag":"\"bbb6030c8316118bcc5c84e5cd79a2d55af4dc83\"","self_link":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","email":"johndoe@gmail.com","verified_on":"2015-06-18T18:10:07.899016","display_name":"None","user":"http://localhost:8001/3.0/users/97071403524618786289886643396072006536","registered_on":"2015-06-17T10:16:24.592951"}],"start":0,"total_size":3}, { date: 'Mon, 22 Jun 2015 18:15:48 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '1243',
  'content-type': 'application/json; charset=utf-8' });