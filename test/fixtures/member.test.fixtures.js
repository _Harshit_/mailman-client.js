var nock = require('nock');

nock('http://localhost:8001')
  .get('/3.0/members/251966682821025451828522645794687675727')
  .times(6)
  .reply(200, {"member_id":2.5196668282102547e+38,"http_etag":"\"a0f7c3a52f5f444e52d8cd782a77972909302d04\"","list_id":"example_list.domain.org","delivery_mode":"regular","address":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","role":"owner","user":"http://localhost:8001/3.0/users/133826164605064147732877946476680872392","email":"johndoe@gmail.com","self_link":"http://localhost:8001/3.0/members/251966682821025451828522645794687675727"}, { date: 'Mon, 27 Jul 2015 23:35:26 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '493',
  'content-type': 'application/json; charset=utf-8' });

nock('http://localhost:8001')
  .get('/3.0/members/251966682821025451828522645794687675727')
  .reply(200, {"member_id":2.5196668282102547e+38,"http_etag":"\"a0f7c3a52f5f444e52d8cd782a77972909302d04\"","list_id":"example_list.domain.org","delivery_mode":"regular","address":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","role":"owner","user":"http://localhost:8001/3.0/users/133826164605064147732877946476680872392","email":"johndoe@gmail.com","self_link":"http://localhost:8001/3.0/members/251966682821025451828522645794687675727"}, { date: 'Mon, 27 Jul 2015 23:35:26 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '493',
  'content-type': 'application/json; charset=utf-8' });

nock('http://localhost:8001')
  .delete('/3.0/members/251966682821025451828522645794687675727')
  .reply(204, "", { date: 'Tue, 28 Jul 2015 00:37:37 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });


