var nock = require('nock');

nock('http://localhost:8001')
  .get('/3.0/domains/somedomain.org')
  .times(6)
  .reply(200, {"mail_host":"somedomain.org","base_url":"http://somedomain.org","url_host":"somedomain.org","self_link":"http://localhost:8001/3.0/domains/somedomain.org","description":"Testing domain","http_etag":"\"b938728cd8749a240a62657cba2ee2d79819b734\""}, { date: 'Sat, 01 Aug 2015 06:57:26 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '249',
  'content-type': 'application/json; charset=utf-8' });

nock('http://localhost:8001')
  .get('/3.0/domains/somedomain.org/lists')
  .reply(200, {"start":0,"total_size":15,"entries":[{"mail_host":"somedomain.org","display_name":"Ankush","list_id":"ankush.somedomain.org","self_link":"http://localhost:8001/3.0/lists/ankush.somedomain.org","list_name":"ankush","volume":1,"fqdn_listname":"ankush@somedomain.org","member_count":0,"http_etag":"\"376d38fdff178317c1786f923ab10a5458067c9e\""},{"mail_host":"somedomain.org","display_name":"Ankush_sharma","list_id":"ankush_sharma.somedomain.org","self_link":"http://localhost:8001/3.0/lists/ankush_sharma.somedomain.org","list_name":"ankush_sharma","volume":1,"fqdn_listname":"ankush_sharma@somedomain.org","member_count":0,"http_etag":"\"54e0f5122f966dbfa8562d480d92726aca685c0d\""},{"mail_host":"somedomain.org","display_name":"Ankush_sharma1","list_id":"ankush_sharma1.somedomain.org","self_link":"http://localhost:8001/3.0/lists/ankush_sharma1.somedomain.org","list_name":"ankush_sharma1","volume":1,"fqdn_listname":"ankush_sharma1@somedomain.org","member_count":0,"http_etag":"\"19ee4755f79a04f56b390bb1dba64cb889169348\""},{"mail_host":"somedomain.org","display_name":"Ankush_sharma2","list_id":"ankush_sharma2.somedomain.org","self_link":"http://localhost:8001/3.0/lists/ankush_sharma2.somedomain.org","list_name":"ankush_sharma2","volume":1,"fqdn_listname":"ankush_sharma2@somedomain.org","member_count":0,"http_etag":"\"d9381615a3fb68dbfd655bfde54d14c773be93e7\""},{"mail_host":"somedomain.org","display_name":"Ankush_sharma3","list_id":"ankush_sharma3.somedomain.org","self_link":"http://localhost:8001/3.0/lists/ankush_sharma3.somedomain.org","list_name":"ankush_sharma3","volume":1,"fqdn_listname":"ankush_sharma3@somedomain.org","member_count":0,"http_etag":"\"ea378d6991f7cb06fc57837e28c7cc8b242051f9\""},{"mail_host":"somedomain.org","display_name":"Ankush_sharma4","list_id":"ankush_sharma4.somedomain.org","self_link":"http://localhost:8001/3.0/lists/ankush_sharma4.somedomain.org","list_name":"ankush_sharma4","volume":1,"fqdn_listname":"ankush_sharma4@somedomain.org","member_count":0,"http_etag":"\"9b74f9104423da3d792a041abe0c8d31a1755c73\""},{"mail_host":"somedomain.org","display_name":"Ankush_sharma5","list_id":"ankush_sharma5.somedomain.org","self_link":"http://localhost:8001/3.0/lists/ankush_sharma5.somedomain.org","list_name":"ankush_sharma5","volume":1,"fqdn_listname":"ankush_sharma5@somedomain.org","member_count":0,"http_etag":"\"fc43043d13c221b31f7455a0d06fa18156d28524\""},{"mail_host":"somedomain.org","display_name":"Black_perl","list_id":"black_perl.somedomain.org","self_link":"http://localhost:8001/3.0/lists/black_perl.somedomain.org","list_name":"black_perl","volume":1,"fqdn_listname":"black_perl@somedomain.org","member_count":0,"http_etag":"\"b804d45faa77a7418a2a9e03030cc5a29becd306\""},{"mail_host":"somedomain.org","display_name":"Black_perl1","list_id":"black_perl1.somedomain.org","self_link":"http://localhost:8001/3.0/lists/black_perl1.somedomain.org","list_name":"black_perl1","volume":1,"fqdn_listname":"black_perl1@somedomain.org","member_count":0,"http_etag":"\"6a76323ce52860aabbc700244406b2874e4d6040\""},{"mail_host":"somedomain.org","display_name":"Black_perl2","list_id":"black_perl2.somedomain.org","self_link":"http://localhost:8001/3.0/lists/black_perl2.somedomain.org","list_name":"black_perl2","volume":1,"fqdn_listname":"black_perl2@somedomain.org","member_count":0,"http_etag":"\"c10a7655a01216eb37fa98c9e871417ee04fcb41\""},{"mail_host":"somedomain.org","display_name":"Black_perl3","list_id":"black_perl3.somedomain.org","self_link":"http://localhost:8001/3.0/lists/black_perl3.somedomain.org","list_name":"black_perl3","volume":1,"fqdn_listname":"black_perl3@somedomain.org","member_count":0,"http_etag":"\"2f3d1d73f3a8df9fbccdf0d7c1078e074d99af9e\""},{"mail_host":"somedomain.org","display_name":"Black_perl4","list_id":"black_perl4.somedomain.org","self_link":"http://localhost:8001/3.0/lists/black_perl4.somedomain.org","list_name":"black_perl4","volume":1,"fqdn_listname":"black_perl4@somedomain.org","member_count":0,"http_etag":"\"2638d15423d5ebc03c0c1ce43cc87c37c8eb3931\""},{"mail_host":"somedomain.org","display_name":"Black_perl5","list_id":"black_perl5.somedomain.org","self_link":"http://localhost:8001/3.0/lists/black_perl5.somedomain.org","list_name":"black_perl5","volume":1,"fqdn_listname":"black_perl5@somedomain.org","member_count":0,"http_etag":"\"b38c965546b55d80e79bf9670098b0bce9b5a755\""},{"mail_host":"somedomain.org","display_name":"Black_perl7","list_id":"black_perl7.somedomain.org","self_link":"http://localhost:8001/3.0/lists/black_perl7.somedomain.org","list_name":"black_perl7","volume":1,"fqdn_listname":"black_perl7@somedomain.org","member_count":0,"http_etag":"\"5a089c5378020f52e7a49483652e7b5340e85324\""},{"mail_host":"somedomain.org","display_name":"List_2","list_id":"list_2.somedomain.org","self_link":"http://localhost:8001/3.0/lists/list_2.somedomain.org","list_name":"list_2","volume":1,"fqdn_listname":"list_2@somedomain.org","member_count":0,"http_etag":"\"87b1483f06458ed08fb31c309a7b57ce72fdae36\""}],"http_etag":"\"a74a99798fb08ef8b480624b684b640c202e6a83\""}, { date: 'Sat, 01 Aug 2015 06:57:26 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '5234',
  'content-type': 'application/json; charset=utf-8' });

nock('http://localhost:8001')
  .post('/3.0/lists', "fqdn_listname=foolist%40somedomain.org")
  .reply(201, "", { date: 'Sat, 01 Aug 2015 06:57:29 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  location: 'http://localhost:8001/3.0/lists/foolist.somedomain.org',
  'content-length': '0' });

nock('http://localhost:8001')
  .delete('/3.0/domains/somedomain.org/owners')
  .reply(204, "", { date: 'Sat, 01 Aug 2015 06:57:29 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });

nock('http://localhost:8001')
  .post('/3.0/domains/somedomain.org/owners', "owner=anne%40gmail.com")
  .reply(204, "", { date: 'Sat, 01 Aug 2015 06:57:32 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });

nock('http://localhost:8001')
  .get('/3.0/domains/somedomain.org/owners')
  .reply(200, {"start":0,"total_size":1,"entries":[{"self_link":"http://localhost:8001/3.0/users/95782073327851546303339963414744746592","user_id":9.578207332785154e+37,"created_on":"2015-08-01T06:57:31.738095","is_server_owner":false,"http_etag":"\"6343c9453909ac69cde31d473e6e2faf06625034\""}],"http_etag":"\"bcec4e9e5cd31a6d0a8b173daf39795bd348bd46\""}, { date: 'Sat, 01 Aug 2015 07:24:54 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '374',
  'content-type': 'application/json; charset=utf-8' });



