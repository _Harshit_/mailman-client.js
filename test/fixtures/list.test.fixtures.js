var nock = require('nock');

nock('http://localhost:8001')
  .get('/3.0/lists/black_perl.example1.org')
  .times(30)
  .reply(200, {"http_etag":"\"46472b053f2b882fd10df6e26d00e88a90773e08\"","volume":1,"display_name":"Black_perl","mail_host":"example1.org","member_count":2,"list_id":"black_perl.example1.org","fqdn_listname":"black_perl@example1.org","list_name":"black_perl","self_link":"http://localhost:8001/3.0/lists/black_perl.example1.org"}, { date: 'Mon, 03 Aug 2015 14:37:54 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '333' });

nock('http://localhost:8001')
  .get('/3.0/lists/black_perl.example1.org/roster/owner')
  .reply(200, {"start":0,"http_etag":"\"ceb9cfd4ed7b14ea0e83bf63e44cbd5528b97d54\"","entries":[{"http_etag":"\"8d316dc0b1964092cccbb4f2fe581edff39baa53\"","list_id":"black_perl.example1.org","address":"http://localhost:8001/3.0/addresses/jacksmith@gmail.com","user":"http://localhost:8001/3.0/users/189001718136851253608364553795677612444","role":"owner","member_id":3.2977914764244658e+38,"delivery_mode":"regular","self_link":"http://localhost:8001/3.0/members/329779147642446573875499578076802818185","email":"jacksmith@gmail.com"},{"http_etag":"\"f46fb5ce000c44222bbdbade758a3b0562b2aebf\"","list_id":"black_perl.example1.org","address":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","user":"http://localhost:8001/3.0/users/237359185796576790161882735856376731152","role":"owner","member_id":4.325141919465715e+37,"delivery_mode":"regular","self_link":"http://localhost:8001/3.0/members/43251419194657149365568808429641758023","email":"johndoe@gmail.com"}],"total_size":2}, { date: 'Mon, 03 Aug 2015 14:38:00 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '1047' });

nock('http://localhost:8001')
  .get('/3.0/lists/black_perl.example1.org/roster/moderator')
  .reply(200, {"start":0,"http_etag":"\"09f2473f9ab2cbc1db3bf12ef09c7a2858ff21a1\"","entries":[{"http_etag":"\"5cbfdbfc1a03b7d50b152901141c0ba7a4bbe72e\"","list_id":"black_perl.example1.org","address":"http://localhost:8001/3.0/addresses/caroljames@gmail.com","user":"http://localhost:8001/3.0/users/252019449059941878788914358144405921204","role":"moderator","member_id":2.033819578034458e+38,"delivery_mode":"regular","self_link":"http://localhost:8001/3.0/members/203381957803445777197001930218336390412","email":"caroljames@gmail.com"}],"total_size":1}, { date: 'Mon, 03 Aug 2015 14:38:00 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '584' });

nock('http://localhost:8001')
  .get('/3.0/lists/black_perl@example1.org/roster/member')
  .times(2)
  .reply(200, {"start":0,"http_etag":"\"2d5951482135c5efead93ec90915e911eb187965\"","entries":[{"http_etag":"\"011998730073bc4bb578a94de9b70779bfcfa325\"","list_id":"black_perl.example1.org","address":"http://localhost:8001/3.0/addresses/annefrank@gmail.com","user":"http://localhost:8001/3.0/users/253931212530303250947529697422451859609","role":"member","member_id":1.071057233999675e+38,"delivery_mode":"regular","self_link":"http://localhost:8001/3.0/members/107105723399967503049225874702952776742","email":"annefrank@gmail.com"},{"http_etag":"\"7629d2293455d36c8f2cdb97c81bd3044a866ca3\"","list_id":"black_perl.example1.org","address":"http://localhost:8001/3.0/addresses/bobwills@gmail.com","user":"http://localhost:8001/3.0/users/248716208401911049429558747538741012742","role":"member","member_id":1.3790596835635758e+38,"delivery_mode":"regular","self_link":"http://localhost:8001/3.0/members/137905968356357575587891316894027841799","email":"bobwills@gmail.com"}],"total_size":2}, { date: 'Mon, 03 Aug 2015 14:38:00 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '1053' });

nock('http://localhost:8001')
  .post('/3.0/members/find', "role=nonmember&list_id=black_perl.example1.org")
  .reply(200, {"start":0,"http_etag":"\"32223434a0f3af4cdc4673d1fbc5bac1f6d98fd3\"","total_size":0}, { date: 'Mon, 03 Aug 2015 14:38:00 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '90' });

nock('http://localhost:8001')
  .get('/3.0/lists/black_perl@example1.org/held')
  .reply(200, {"start":0,"http_etag":"\"32223434a0f3af4cdc4673d1fbc5bac1f6d98fd3\"","total_size":0}, { date: 'Mon, 03 Aug 2015 14:38:02 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '90' });

nock('http://localhost:8001')
  .get('/3.0/lists/black_perl@example1.org/requests')
  .reply(200, {"start":0,"http_etag":"\"32223434a0f3af4cdc4673d1fbc5bac1f6d98fd3\"","total_size":0}, { date: 'Mon, 03 Aug 2015 14:38:02 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '90' });

nock('http://localhost:8001')
  .post('/3.0/members', "list_id=black_perl.example1.org&subscriber=johndoe%40gmail.com&role=owner")
  .reply(201, "", { date: 'Mon, 03 Aug 2015 14:38:02 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'text/plain',
  'content-length': '0' });

nock('http://localhost:8001')
  .post('/3.0/members', "list_id=black_perl.example1.org&subscriber=johndoe%40gmail.com&role=moderator")
  .reply(201, "", { date: 'Mon, 03 Aug 2015 14:38:02 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'text/plain',
  'content-length': '0' });

nock('http://localhost:8001')
  .delete('/3.0/lists/black_perl@example1.org/moderator/caroljames@gmail.com')
  .reply(204, "", { date: 'Mon, 03 Aug 2015 14:38:02 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });

nock('http://localhost:8001')
  .delete('/3.0/lists/black_perl@example1.org/owner/jacksmith@gmail.com')
  .reply(204, "", { date: 'Mon, 03 Aug 2015 14:38:03 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });

nock('http://localhost:8001')
  .get('/3.0/members/107105723399967503049225874702952776742')
  .reply(200,{"http_etag": "\"011998730073bc4bb578a94de9b70779bfcfa325\"", "list_id": "black_perl.example1.org", "address": "http://localhost:8001/3.0/addresses/annefrank@gmail.com", "user": "http://localhost:8001/3.0/users/253931212530303250947529697422451859609", "role": "member", "member_id": 107105723399967503049225874702952776742, "delivery_mode": "regular", "self_link": "http://localhost:8001/3.0/members/107105723399967503049225874702952776742", "email": "annefrank@gmail.com"},{ date: 'Mon, 03 Aug 2015 14:38:03 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json',
  'content-length': '474' });

nock('http://localhost:8001')
  .get('/3.0/members/137905968356357575587891316894027841799')
  .reply(200,{"http_etag": "\"7629d2293455d36c8f2cdb97c81bd3044a866ca3\"", "list_id": "black_perl.example1.org", "address": "http://localhost:8001/3.0/addresses/bobwills@gmail.com", "user": "http://localhost:8001/3.0/users/248716208401911049429558747538741012742", "role": "member", "member_id": 137905968356357575587891316894027841799, "delivery_mode": "regular", "self_link": "http://localhost:8001/3.0/members/137905968356357575587891316894027841799", "email": "bobwills@gmail.com"},{ date: 'Mon, 03 Aug 2015 14:38:03 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json',
  'content-length': '472' });

nock('http://localhost:8001')
  .delete('/3.0/lists/black_perl@example1.org')
  .reply(204, "", { date: 'Mon, 03 Aug 2015 14:38:02 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });







