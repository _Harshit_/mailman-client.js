var nock = require('nock');

nock('http://localhost:8001')
  .get('/3.0/system/versions')
  .reply(200, {"python_version":"3.4.0 (default, Apr 11 2014, 13:05:11) \n[GCC 4.8.2]","http_etag":"\"0fb479269f9ddce17180453445bd78b560c02061\"","mailman_version":"GNU Mailman 3.1.0 (Between The Wheels)","self_link":"http://localhost:8001/3.0/system/versions"}, { date: 'Sat, 01 Aug 2015 19:59:45 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '254' });

nock('http://localhost:8001')
  .get('/3.0/lists')
  .reply(200, {"start":0,"total_size":17,"http_etag":"\"6193267b9f63cc34193904a55749819d2d9e8412\"","entries":[{"member_count":0,"display_name":"List_2","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/list_2.example1.org","fqdn_listname":"list_2@example1.org","list_id":"list_2.example1.org","http_etag":"\"87b1483f06458ed08fb31c309a7b57ce72fdae36\"","list_name":"list_2","volume":1},{"member_count":0,"display_name":"List_4","mail_host":"example2.org","self_link":"http://localhost:8001/3.0/lists/list_4.example2.org","fqdn_listname":"list_4@example2.org","list_id":"list_4.example2.org","http_etag":"\"ba516f01a494004646cea53cebc1d3091230513b\"","list_name":"list_4","volume":1},{"member_count":0,"display_name":"Black_perl","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/black_perl.example1.org","fqdn_listname":"black_perl@example1.org","list_id":"black_perl.example1.org","http_etag":"\"b804d45faa77a7418a2a9e03030cc5a29becd306\"","list_name":"black_perl","volume":1},{"member_count":0,"display_name":"Black_perl1","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/black_perl1.example1.org","fqdn_listname":"black_perl1@example1.org","list_id":"black_perl1.example1.org","http_etag":"\"6a76323ce52860aabbc700244406b2874e4d6040\"","list_name":"black_perl1","volume":1},{"member_count":0,"display_name":"Black_perl2","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/black_perl2.example1.org","fqdn_listname":"black_perl2@example1.org","list_id":"black_perl2.example1.org","http_etag":"\"c10a7655a01216eb37fa98c9e871417ee04fcb41\"","list_name":"black_perl2","volume":1},{"member_count":0,"display_name":"Black_perl3","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/black_perl3.example1.org","fqdn_listname":"black_perl3@example1.org","list_id":"black_perl3.example1.org","http_etag":"\"2f3d1d73f3a8df9fbccdf0d7c1078e074d99af9e\"","list_name":"black_perl3","volume":1},{"member_count":0,"display_name":"Black_perl4","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/black_perl4.example1.org","fqdn_listname":"black_perl4@example1.org","list_id":"black_perl4.example1.org","http_etag":"\"2638d15423d5ebc03c0c1ce43cc87c37c8eb3931\"","list_name":"black_perl4","volume":1},{"member_count":0,"display_name":"Black_perl5","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/black_perl5.example1.org","fqdn_listname":"black_perl5@example1.org","list_id":"black_perl5.example1.org","http_etag":"\"b38c965546b55d80e79bf9670098b0bce9b5a755\"","list_name":"black_perl5","volume":1},{"member_count":0,"display_name":"Black_perl7","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/black_perl7.example1.org","fqdn_listname":"black_perl7@example1.org","list_id":"black_perl7.example1.org","http_etag":"\"5a089c5378020f52e7a49483652e7b5340e85324\"","list_name":"black_perl7","volume":1},{"member_count":0,"display_name":"Ankush","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/ankush.example1.org","fqdn_listname":"ankush@example1.org","list_id":"ankush.example1.org","http_etag":"\"376d38fdff178317c1786f923ab10a5458067c9e\"","list_name":"ankush","volume":1},{"member_count":0,"display_name":"Ankush_sharma","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/ankush_sharma.example1.org","fqdn_listname":"ankush_sharma@example1.org","list_id":"ankush_sharma.example1.org","http_etag":"\"54e0f5122f966dbfa8562d480d92726aca685c0d\"","list_name":"ankush_sharma","volume":1},{"member_count":0,"display_name":"Ankush_sharma1","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/ankush_sharma1.example1.org","fqdn_listname":"ankush_sharma1@example1.org","list_id":"ankush_sharma1.example1.org","http_etag":"\"19ee4755f79a04f56b390bb1dba64cb889169348\"","list_name":"ankush_sharma1","volume":1},{"member_count":0,"display_name":"Ankush_sharma2","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/ankush_sharma2.example1.org","fqdn_listname":"ankush_sharma2@example1.org","list_id":"ankush_sharma2.example1.org","http_etag":"\"d9381615a3fb68dbfd655bfde54d14c773be93e7\"","list_name":"ankush_sharma2","volume":1},{"member_count":0,"display_name":"Ankush_sharma3","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/ankush_sharma3.example1.org","fqdn_listname":"ankush_sharma3@example1.org","list_id":"ankush_sharma3.example1.org","http_etag":"\"ea378d6991f7cb06fc57837e28c7cc8b242051f9\"","list_name":"ankush_sharma3","volume":1},{"member_count":0,"display_name":"Ankush_sharma4","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/ankush_sharma4.example1.org","fqdn_listname":"ankush_sharma4@example1.org","list_id":"ankush_sharma4.example1.org","http_etag":"\"9b74f9104423da3d792a041abe0c8d31a1755c73\"","list_name":"ankush_sharma4","volume":1},{"member_count":0,"display_name":"Ankush_sharma5","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/ankush_sharma5.example1.org","fqdn_listname":"ankush_sharma5@example1.org","list_id":"ankush_sharma5.example1.org","http_etag":"\"fc43043d13c221b31f7455a0d06fa18156d28524\"","list_name":"ankush_sharma5","volume":1},{"member_count":0,"display_name":"Foolist","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/foolist.example1.org","fqdn_listname":"foolist@example1.org","list_id":"foolist.example1.org","http_etag":"\"031a4c00bed9b4c387316e27be836f01ed3c005c\"","list_name":"foolist","volume":1}]}, { date: 'Sat, 01 Aug 2015 19:59:45 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '5869' });

nock('http://localhost:8001')
  .get('/3.0/domains')
  .times(2)
  .reply(200, {"start":0,"total_size":3,"http_etag":"\"ba57d7f8884aaf4f92328d59f790b643596811fd\"","entries":[{"mail_host":"black-perl.com","self_link":"http://localhost:8001/3.0/domains/black-perl.com","description":"The domain is for testing purposes","url_host":"","base_url":"www.black-perl.com","http_etag":"\"bb89e0eec402adfd3f4eb0b4b027a73fe022670a\""},{"mail_host":"example1.org","self_link":"http://localhost:8001/3.0/domains/example1.org","description":"Testing domain","url_host":"example1.org","base_url":"http://example1.org","http_etag":"\"b938728cd8749a240a62657cba2ee2d79819b734\""},{"mail_host":"example2.org","self_link":"http://localhost:8001/3.0/domains/example2.org","description":"Testing domain 2","url_host":"example2.org","base_url":"http://example2.org","http_etag":"\"439e2afe7d805a8354e51792dd87519a6a183daf\""}]}, { date: 'Sat, 01 Aug 2015 19:59:45 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '869' });

nock('http://localhost:8001')
  .get('/3.0/members')
  .reply(200, {"start":0,"total_size":1,"http_etag":"\"34b29b22a92d505328556a5bc657b2c997be3be1\"","entries":[{"delivery_mode":"regular","role":"owner","member_id":1.0689503344204875e+38,"self_link":"http://localhost:8001/3.0/members/106895033442048746754783910678565967857","list_id":"list_2.example1.org","http_etag":"\"2ecda62a61f8b81b39096d9c060e31d56d6e9f53\"","user":"http://localhost:8001/3.0/users/133477693148359738306788754891293159234","email":"ankush@gmail.com","address":"http://localhost:8001/3.0/addresses/ankush@gmail.com"}]}, { date: 'Sat, 01 Aug 2015 19:59:46 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '568' });

nock('http://localhost:8001')
  .get('/3.0/users')
  .reply(200, {"start":0,"total_size":14,"http_etag":"\"a3a853dce2312c00d3be95cd109a8f39ff30daa6\"","entries":[{"display_name":"None","created_on":"2015-06-17T17:08:27.965655","self_link":"http://localhost:8001/3.0/users/184305674337266952708725330371285589612","user_id":1.8430567433726695e+38,"http_etag":"\"501dfc435f9427ea7294b75ea96d64c6cbf826ef\"","is_server_owner":false},{"display_name":"ankush","created_on":"2015-07-16T11:23:07.981607","self_link":"http://localhost:8001/3.0/users/133477693148359738306788754891293159234","password":"$6$rounds=98858$MFnVPmvheF2IsiST$wwgyCvuAW7wX4zZ99zJPewztNbfz9W16vIAXQ.kESV/4EdvoslWCmW8/QtSxJpLIwpDbLo4tJDtmTeVWj.jyD.","user_id":1.3347769314835973e+38,"http_etag":"\"a199b59a624e2068841242203a687b73c7d7eef6\"","is_server_owner":false},{"display_name":"None","created_on":"2015-07-16T11:25:23.458710","self_link":"http://localhost:8001/3.0/users/133826164605064147732877946476680872392","user_id":1.3382616460506414e+38,"http_etag":"\"6fb20761024fe8dc7887b7ca9d95794b81f9ff16\"","is_server_owner":false},{"user_id":1.1111542056217387e+38,"created_on":"2015-07-16T22:25:39.832719","http_etag":"\"eb9aaeda6e737444e6673d078525c189f92340fe\"","self_link":"http://localhost:8001/3.0/users/111115420562173869860933341759294105578","is_server_owner":false},{"user_id":3.2677316139139358e+38,"created_on":"2015-07-16T22:26:45.851005","http_etag":"\"30865e2e0732f1ca2816d9db443787e96febe90e\"","self_link":"http://localhost:8001/3.0/users/326773161391393563102064443238398455994","is_server_owner":false},{"user_id":2.37732839384361e+38,"created_on":"2015-07-16T22:27:25.553655","http_etag":"\"09e82de5fb115b69cc8ae9be1b964090a599c4fb\"","self_link":"http://localhost:8001/3.0/users/237732839384360998698347345442361210633","is_server_owner":false},{"display_name":"black-perl","created_on":"2015-07-16T23:04:44.945338","self_link":"http://localhost:8001/3.0/users/3953201358285736770572888270338091298","user_id":3.953201358285737e+36,"http_etag":"\"b34e7ee62bee11b0062389e2bc628db249ba76e3\"","is_server_owner":false},{"display_name":"black-perl","created_on":"2015-07-17T12:36:16.957890","self_link":"http://localhost:8001/3.0/users/187205570503872505744861643056806781323","user_id":1.8720557050387252e+38,"http_etag":"\"1f269543d873412a11e7cc10d50c4ef156db283b\"","is_server_owner":false},{"display_name":"ankush2","created_on":"2015-07-17T14:16:48.207244","self_link":"http://localhost:8001/3.0/users/306010936727116810032849488928788242957","password":"$6$rounds=90778$/Co5ps7wXQa9m2Hf$3JllYifAcrPWKDCB/2CaEu1kGQWLf0o8jqFS0wGzrITBKbMn6qKdLoGA..iArCAQxW2uZmVMx0/ybHKluBJRt.","user_id":3.060109367271168e+38,"http_etag":"\"d65691692c2f0f2a7566e444b933dd3686ba8022\"","is_server_owner":false},{"display_name":"ankush2","created_on":"2015-07-17T14:35:46.952217","self_link":"http://localhost:8001/3.0/users/101416523094530824358779205223776645184","password":"$6$rounds=104917$bCF5eM8lw/L9qgAn$RkCf3iBn9L0kSlP8Ksu4u9sffYxv0Vgsw/F/TN741w80KlaEPQaIFkTvcIttFWoOE.GDL0/5JuMYwj4G1tUgc.","user_id":1.0141652309453082e+38,"http_etag":"\"96ff972f62072dcd65a186b37dc44940eae2e05b\"","is_server_owner":false},{"display_name":"ankush2","created_on":"2015-07-17T14:36:37.511787","self_link":"http://localhost:8001/3.0/users/294155925345219420782272019876823849597","password":"$6$rounds=91932$YLGgpeZmglB/Zh8i$VuQ1Grsdruk5wwahLZ3bmUfRx/VIbDh2IHRkxNDS0AiesMH5smhh7X1FRoSd9fkdbs9vQcw2FVRjYNcmXdkGT/","user_id":2.9415592534521942e+38,"http_etag":"\"280e39beae7a54f91aa0b9c463ff346cf25fd1f7\"","is_server_owner":false},{"display_name":"ankush2","created_on":"2015-07-17T14:36:57.496651","self_link":"http://localhost:8001/3.0/users/268087644924434482223868414877300044675","password":"$6$rounds=98422$OHtN9mDjBwwzz8DT$/WKJyrTwZx9nURDlYuahO5tWoGBhFbZ9hjSRTwKfqehBtyGPBzW9cn1sO3aNmM2Z/7AYy3Q2uF2S/DZNyy0Ds1","user_id":2.6808764492443446e+38,"http_etag":"\"94fe3cab636adb963db3e06abe94a214e3b40b33\"","is_server_owner":false},{"user_id":2.6462824444178406e+38,"created_on":"2015-07-24T07:37:52.555457","http_etag":"\"3d730defda44184d2e5ea6f2d898992a3f0347f4\"","self_link":"http://localhost:8001/3.0/users/264628244441784063104614701063595340001","is_server_owner":false},{"user_id":9.578207332785154e+37,"created_on":"2015-08-01T06:57:31.738095","http_etag":"\"6343c9453909ac69cde31d473e6e2faf06625034\"","self_link":"http://localhost:8001/3.0/users/95782073327851546303339963414744746592","is_server_owner":false}]}, { date: 'Sat, 01 Aug 2015 19:59:46 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '4838' });

nock('http://localhost:8001')
  .post('/3.0/domains', "mail_host=foobar.com&base_url=http%3A%2F%2Ffoobar.com&description=Foo%20bar&owner=bob%40foomail.com")
  .reply(201, "", { date: 'Sat, 01 Aug 2015 19:59:48 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  location: 'http://localhost:8001/3.0/domains/foobar.com',
  'content-length': '0' });

nock('http://localhost:8001')
  .delete('/3.0/domains/example2.org')
  .reply(204, "", { date: 'Sat, 01 Aug 2015 19:59:50 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });

nock('http://localhost:8001')
  .get('/3.0/domains/black-perl.com')
  .reply(200, {"mail_host":"black-perl.com","self_link":"http://localhost:8001/3.0/domains/black-perl.com","description":"The domain is for testing purposes","url_host":"","base_url":"www.black-perl.com","http_etag":"\"bb89e0eec402adfd3f4eb0b4b027a73fe022670a\""}, { date: 'Sat, 01 Aug 2015 19:59:54 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '260' });

nock('http://localhost:8001')
  .get('/3.0/domains/example1.org')
  .reply(200, {"mail_host":"example1.org","self_link":"http://localhost:8001/3.0/domains/example1.org","description":"Testing domain","url_host":"example1.org","base_url":"http://example1.org","http_etag":"\"b938728cd8749a240a62657cba2ee2d79819b734\""}, { date: 'Sat, 01 Aug 2015 19:59:54 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '249' });

nock('http://localhost:8001')
  .get('/3.0/domains/example2.org')
  .times(3)
  .reply(200, {"mail_host":"example2.org","self_link":"http://localhost:8001/3.0/domains/example2.org","description":"Foo bar","url_host":"example2.org","base_url":"http://example2.org","http_etag":"\"b629482b9e94bde098aa91cda6cb45deac10e003\""}, { date: 'Sat, 01 Aug 2015 19:59:54 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '234' });

nock('http://localhost:8001')
  .post('/3.0/users', "email=bob%40gmail.com&password=unguessable&display_name=bob%20frank")
  .reply(201, "", { date: 'Sat, 01 Aug 2015 19:59:56 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  location: 'http://localhost:8001/3.0/users/281585453841779272584684797556119798399',
  'content-length': '0' });

nock('http://localhost:8001')
  .get('/3.0/users/bobwills@gmail.com')
  .reply(200, {"created_on": "2015-07-16T11:25:23.458710", "user_id": 133826164605064147732877946476680872392, "self_link": "http://localhost:8001/3.0/users/133826164605064147732877946476680872392", "http_etag": "\"6fb20761024fe8dc7887b7ca9d95794b81f9ff16\"", "display_name": "None", "is_server_owner": false}, { date: 'Sat, 01 Aug 2015 19:59:56 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '295' });

nock('http://localhost:8001')
  .get('/3.0/addresses/johndoe@yahoo.com')
  .reply(200, {"self_link":"http://localhost:8001/3.0/addresses/johndoe@yahoo.com","verified_on":"2015-07-16T11:25:23.482002","http_etag":"\"4c0354dfc166a7bd54c65fa8a5de429f54c2a998\"","user":"http://localhost:8001/3.0/users/133826164605064147732877946476680872392","email":"johndoe@yahoo.com","display_name":"None","registered_on":"2015-07-16T11:25:23.446105","original_email":"johndoe@yahoo.com"}, { date: 'Sun, 02 Aug 2015 06:10:39 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '441',
  'content-type': 'application/json; charset=utf-8' });

nock('http://localhost:8001')
  .get('/3.0/lists/list_2@example1.org')
  .reply(200, {"member_count":0,"display_name":"List_2","mail_host":"example1.org","self_link":"http://localhost:8001/3.0/lists/list_2.example1.org","fqdn_listname":"list_2@example1.org","list_id":"list_2.example1.org","http_etag":"\"87b1483f06458ed08fb31c309a7b57ce72fdae36\"","list_name":"list_2","volume":1}, { date: 'Sat, 01 Aug 2015 20:00:00 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '313' });

nock('http://localhost:8001')
  .delete('/3.0/lists/list_2@example1.org')
  .reply(204, "", { date: 'Sat, 01 Aug 2015 06:57:29 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });


