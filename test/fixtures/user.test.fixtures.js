var nock = require('nock');

nock('http://localhost:8001')
  .get('/3.0/users/149132891060733238619497360715436428427')
  .times(10)  
  .reply(200, {"created_on":"2015-06-17T17:05:37.741005","user_id":149132891060733238619497360715436428427,"http_etag":"\"bdf233fb9367a8ea069142db3fd8fd24e201eead\"","is_server_owner":false,"password":"$6$rounds=96671$qHl/Gpb.BrSqko8c$/mBbW.Iv4AGoxWvW95puHWkFTvD7UDl2DzrSQmQ7HlTe2FjUkKantkaJxiq0dfA4qrCIIeORGgeHGnfXN9GiO.","display_name":"black-perl","self_link":"http://localhost:8001/3.0/users/149132891060733238619497360715436428427"}, { date: 'Sun, 26 Jul 2015 11:53:09 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '436' });

nock('http://localhost:8001')
  .post('/3.0/users/149132891060733238619497360715436428427/addresses', "email=testemail%40gmail.com")
  .reply(201, "", { date: 'Mon, 27 Jul 2015 00:00:03 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  location: 'http://localhost:8001/3.0/addresses/testemail@gmail.com',
  'content-length': '0' });

nock('http://localhost:8001')
  .get('/3.0/users/149132891060733238619497360715436428427/addresses')
  .times(2)
  .reply(200, {"http_etag":"\"915491d2e0a3d36a315953fcb77ce8e206aabbea\"","total_size":2,"entries":[{"email":"johndoe@gmail.com","http_etag":"\"fd495c92f0bcdf675e672a5ea06cf16709a74bb5\"","original_email":"johndoe@gmail.com","user":"http://localhost:8001/3.0/users/149132891060733238619497360715436428427","self_link":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","registered_on":"2015-06-17T17:05:37.740754"},{"email":"johndoe@yahoo.com","http_etag":"\"2e0292b921420dc18274f8173f85c3eccc9bbd46\"","original_email":"johndoe@yahoo.com","user":"http://localhost:8001/3.0/users/149132891060733238619497360715436428427","self_link":"http://localhost:8001/3.0/addresses/johndoe@yahoo.com","registered_on":"2015-07-27T00:00:03.386243"}],"start":0}, { date: 'Mon, 27 Jul 2015 00:51:30 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '782' });

nock('http://localhost:8001')
  .post('/3.0/members/find', "subscriber=johndoe%40yahoo.com")
  .times(2)
  .reply(200, {"http_etag":"\"32223434a0f3af4cdc4673d1fbc5bac1f6d98fd3\"","total_size":0,"start":0}, { date: 'Mon, 27 Jul 2015 00:51:30 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '90' });

nock('http://localhost:8001')
  .post('/3.0/members/find', "subscriber=johndoe%40gmail.com")
  .times(2)
  .reply(200, {"http_etag":"\"4267b7ec76b1fcf5c6d95fae06602de2210d4ce1\"","total_size":4,"entries":[{"email":"johndoe@gmail.com","delivery_mode":"regular","member_id":1.8270872022674814e+38,"list_id":"list_2.example1.org","user":"http://localhost:8001/3.0/users/149132891060733238619497360715436428427","http_etag":"\"586a2661a9433f508473e0cf146d35c00d03c326\"","self_link":"http://localhost:8001/3.0/members/182708720226748139525761445266416568295","address":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","role":"owner"},{"email":"johndoe@gmail.com","delivery_mode":"regular","member_id":1.5267661841740183e+38,"list_id":"list_2.example1.org","user":"http://localhost:8001/3.0/users/149132891060733238619497360715436428427","http_etag":"\"ce707fc47313b64231b9a9764cbe1e4201603576\"","self_link":"http://localhost:8001/3.0/members/152676618417401827803610512929983878275","address":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","role":"moderator"},{"email":"johndoe@gmail.com","delivery_mode":"regular","member_id":3.2300560084124072e+38,"list_id":"list_3.example2.org","user":"http://localhost:8001/3.0/users/149132891060733238619497360715436428427","http_etag":"\"1a9620926d04c6dc2932c66df91fbbed93b5925f\"","self_link":"http://localhost:8001/3.0/members/323005600841240737688921431050789873980","address":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","role":"owner"},{"email":"johndoe@gmail.com","delivery_mode":"regular","member_id":2.297274165057506e+38,"list_id":"list_4.example2.org","user":"http://localhost:8001/3.0/users/149132891060733238619497360715436428427","http_etag":"\"80f2a85bd354770ce460e4d2ae71eb39fcab2a60\"","self_link":"http://localhost:8001/3.0/members/229727416505750597725746288488312764584","address":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","role":"owner"}],"start":0}, { date: 'Mon, 27 Jul 2015 00:51:30 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '1999' });

nock('http://localhost:8001')
  .get('/3.0/members/182708720226748139525761445266416568295')
  .reply(200, {"email":"johndoe@gmail.com","delivery_mode":"regular","member_id":1.8270872022674814e+38,"list_id":"list_2.example1.org","user":"http://localhost:8001/3.0/users/149132891060733238619497360715436428427","http_etag":"\"586a2661a9433f508473e0cf146d35c00d03c326\"","self_link":"http://localhost:8001/3.0/members/182708720226748139525761445266416568295","address":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","role":"owner"}, { date: 'Mon, 27 Jul 2015 11:38:51 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '471' });

nock('http://localhost:8001')
  .get('/3.0/members/152676618417401827803610512929983878275')
  .reply(200, {"email":"johndoe@gmail.com","delivery_mode":"regular","member_id":1.5267661841740183e+38,"list_id":"list_2.example1.org","user":"http://localhost:8001/3.0/users/149132891060733238619497360715436428427","http_etag":"\"ce707fc47313b64231b9a9764cbe1e4201603576\"","self_link":"http://localhost:8001/3.0/members/152676618417401827803610512929983878275","address":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","role":"moderator"}, { date: 'Mon, 27 Jul 2015 11:38:51 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '475' });

nock('http://localhost:8001')
  .get('/3.0/members/323005600841240737688921431050789873980')
  .reply(200, {"email":"johndoe@gmail.com","delivery_mode":"regular","member_id":3.2300560084124072e+38,"list_id":"list_3.example2.org","user":"http://localhost:8001/3.0/users/149132891060733238619497360715436428427","http_etag":"\"1a9620926d04c6dc2932c66df91fbbed93b5925f\"","self_link":"http://localhost:8001/3.0/members/323005600841240737688921431050789873980","address":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","role":"owner"}, { date: 'Mon, 27 Jul 2015 11:38:51 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '471' });

nock('http://localhost:8001')
  .get('/3.0/members/229727416505750597725746288488312764584')
  .reply(200, {"email":"johndoe@gmail.com","delivery_mode":"regular","member_id":2.297274165057506e+38,"list_id":"list_4.example2.org","user":"http://localhost:8001/3.0/users/149132891060733238619497360715436428427","http_etag":"\"80f2a85bd354770ce460e4d2ae71eb39fcab2a60\"","self_link":"http://localhost:8001/3.0/members/229727416505750597725746288488312764584","address":"http://localhost:8001/3.0/addresses/johndoe@gmail.com","role":"owner"}, { date: 'Mon, 27 Jul 2015 11:38:51 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '471' });

nock('http://localhost:8001')
  .patch('/3.0/users/149132891060733238619497360715436428427', "display_name=johnsmith&cleartext_password=youcannotguess")
  .reply(204, "", { date: 'Mon, 27 Jul 2015 11:53:19 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });

nock('http://localhost:8001')
  .delete('/3.0/users/149132891060733238619497360715436428427')
  .reply(204, "", { date: 'Mon, 27 Jul 2015 11:57:36 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });


