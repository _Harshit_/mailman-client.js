'use strict';

/**
Unit tests for the List module
**/

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
chai.use(require('sinon-chai'));
var nock = require('nock');
var path = require('path');

var ApiRequest = require('../lib/shared/ApiRequest.js');
var BaseRequest = require('../lib/shared/BaseRequest.js');
var Member = require('../lib/Member.js');
var Page = require('../lib/Page.js');
var Settings = require('../lib/Settings.js');
var ListArchivers = require('../lib/ListArchivers.js');
var List = require('../lib/List.js');

//nock.recorder.rec()
// loading fixtures
var fixturesPath = path.join(__dirname,'fixtures/list.test.fixtures.js');
try {
    require(fixturesPath);
} catch(e) {
    console.log(e);
    throw new Error('Fixtures file not found');
}


describe('List',function() {
    var listInstance, responseData, options, responseKeys, fakeRequest;

    responseData = { 
        volume: 1,
        mail_host: 'example1.org',
        fqdn_listname: 'black_perl@example1.org',
        display_name: 'Black_perl',
        list_id: 'black_perl.example1.org',
        http_etag: "\"46472b053f2b882fd10df6e26d00e88a90773e08\"",
        self_link: 'http://localhost:8001/3.0/lists/black_perl.example1.org',
        list_name: 'black_perl',
        member_count: 2
    };

    options = {
        endpoint : 'http://localhost:8001/3.0',
        username : 'restadmin',
        password : 'restpass'
    };

    responseKeys = [ 
        'volume',
        'mail_host',
        'fqdn_listname',
        'display_name',
        'list_id',
        'http_etag',
        'self_link',
        'list_name',
        'member_count' 
    ];

    fakeRequest = function() {
        return {
            then : function(cb) { cb() }
        };
    };

    beforeEach(function() {
        listInstance = new List(options,
                                'http://localhost:8001/3.0/lists/black_perl.example1.org');
    });

    describe('constructor', function() {

        it('should be an instance of BaseRequest',function() {
            expect(listInstance).to.be.instanceof(BaseRequest);
        });

        it('should be an instance of ApiRequest',function() {
            expect(listInstance).to.be.instanceof(ApiRequest);
        });

        it('should be an instance of List',function() {
            expect(listInstance).to.be.instanceof(List);
        });

        it('should initialise the instance properties',function() {
            expect(listInstance._supportedMethods).to.be.deep.equal(['get','post','delete','put']);
            expect(listInstance._url).to.be.equal('http://localhost:8001/3.0/lists/black_perl.example1.org');
        });

    });

    describe('.getInfo()',function(done) {

        it('should call the callback with the List instance',function(done) {
            var callback = function(err,data) {
                // pass
            };
            var callbackSpy = sinon.spy(callback);
            listInstance.getInfo(callbackSpy).then(function(retValue) {
                expect(retValue).to.be.deep.equal(listInstance);
                expect(listInstance._info).to.be.deep.equal(responseData);
                expect(callbackSpy).to.be.calledOnce;
                expect(callbackSpy).to.be.calledWith(null,listInstance);
                done();
            });
        });

        it('should provide promise to the listInstance',function(done) {
            listInstance.getInfo().then(function(retValue) {
                expect(retValue).to.be.deep.equal(listInstance);
                expect(listInstance._info).to.be.deep.equal(responseData);
                done();
            });
        });

    }); // .getInfo()

    describe('._setAccessors()',function() {

        it('should set the accessors on the listInstance',function(done) {
            var loopcount = responseKeys.length;
            listInstance.getInfo().then(function() {
                var counter = 0;
                responseKeys.forEach(function(key) {
                    expect(listInstance[key]).to.be.equal(responseData[key]);
                    if ( ++counter == loopcount ) {
                        done();
                    }
                });
            });
        });

    });

    describe('.getOwners()',function() {

        it('should return a promise to the list onwers',function(done) {
             var httpMethodSpy = sinon.spy(listInstance,'get');
            listInstance.getOwners().then(function(retValue) {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl.example1.org/roster/owner');
                retValue.forEach(function(value) {
                    expect(value).to.be.instanceof(Member);
                });
                done();
            });
        });
    
    });

    describe('.getModerators()',function() {

        it('should return a promise to the list owners',function(done) {
             var httpMethodSpy = sinon.spy(listInstance,'get');
            listInstance.getModerators().then(function(retValue) {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl.example1.org/roster/moderator');
                retValue.forEach(function(entry) {
                    expect(entry).to.be.instanceof(Member);
                });
                done();
            });
        });
    
    });

    describe('.getMembers()',function() {

        it('should return a promsie to the list members',function(done) {
            var httpMethodSpy = sinon.spy(listInstance,'get');
            listInstance.getMembers().then(function(retValue) {
                expect(httpMethodSpy).to.be.calledTwice;
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/roster/member');
                retValue.forEach(function(entry) {
                    expect(entry).to.be.instanceof(Member);
                });
                done();
            });
        });

    }); // .getMembers()

    describe('.getNonMembers()',function() {

        it('should return a promise to the non-members of the list',function(done) {
            var httpMethodSpy = sinon.spy(listInstance,'post');
            var payload = {
                role : 'nonmember',
                list_id : 'black_perl.example1.org'
            };
            listInstance.getNonMembers().then(function(retValue) {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(httpMethodSpy).to.be.calledWith(payload);
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/members/find');
                retValue.forEach(function(entry) {
                    expect(entry).to.be.instanceof(Member);
                });
                done();
            });
        });

    }); // .getNonMembers()

    describe('.getMemberPage()',function() {

        it('should return a Page instance corresponding to the list members',function(done) {
            listInstance.getMemberPage().then(function(retValue) {
                expect(retValue).to.be.instanceof(Page);
                expect(retValue._count).to.be.equal(50);
                expect(retValue._page).to.be.equal(1);
                expect(retValue._model).to.be.deep.equal(Member);
                done();
            });
        });

    }); // .getMemberPage

    describe('.getSettings()',function() {

        it('should return a promise to the list settings',function(done) {
            listInstance.getSettings().then(function(retValue) {
                expect(retValue).to.be.instanceof(Settings);
                expect(retValue._template).to.be.equal('/lists/black_perl@example1.org/config');
                done();
            });
        });

    }); // .getSettings()

    describe('.getHeldMessages()',function() {

        it('should provide a promise to an array of held messages objects',function(done) {
            var httpMethodSpy = sinon.spy(listInstance,'get');
            listInstance.getHeldMessages().then(function() {
                expect(httpMethodSpy).to.be.calledTwice;
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/held');
                done();
            });
        });

    }); // .getHeldMessages()

    describe('.getSubscriptionRequests()',function() {

        it('should provide a promise to an array to list subscription requests',function(done) {
            var httpMethodSpy = sinon.spy(listInstance,'get');
            listInstance.getSubscriptionRequests().then(function() {
                expect(httpMethodSpy).to.be.calledTwice;
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/requests');
                done();
            });
        });

    }); // .getSubscriptionRequests()

    describe('.manageRequest()',function() {

        it('should take actions on a subscription request',function(done) {
            var httpMethodSpy = sinon.stub(listInstance,'post',fakeRequest);
            var actionsArray = ['accept','reject','defer','discard'];
            var action = actionsArray[Math.floor(Math.random()*actionsArray.length)];
            var randomToken = '501dfc435f9427ea7294b75ea96d64c6cbf826ef';
            var payload = {
                action : action
            };
            listInstance.manageRequest(randomToken,action).then(function(retValue) {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(httpMethodSpy).to.be.calledWith(payload);
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl.example1.org/requests/' + randomToken);
                expect(retValue).to.be.deep.equal(listInstance);
                done();
            });
        });

    }); // .mamageRequest()

    describe('.getArchivers()',function() {

        it('should return the ListArchivers corresponding to the List object',function(done) {
            listInstance.getArchivers().then(function(retValue) {
                expect(retValue).to.be.instanceof(ListArchivers);
                expect(retValue._listId).to.be.equal('black_perl.example1.org');
                done();
            });
        });

    }); // .getArchivers()

    describe('.addOwner()',function() {

        it('should assign a role of owner to an address corresponding to a list',function(done) {
            var httpMethodSpy = sinon.spy(listInstance,'post');
            var payload = {
                list_id : 'black_perl.example1.org',
                subscriber : 'johndoe@gmail.com',
                role : 'owner'
            };
            listInstance.addOwner('johndoe@gmail.com').then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/members');
                done();
            });
        });

    }); // .addOnwer()

    describe('.addModerator()',function() {

        it('should assign a role of moderator to an address corresponding to a list',function(done) {
            var httpMethodSpy = sinon.spy(listInstance,'post');
            var payload = {
                list_id : 'black_perl.example1.org',
                subscriber : 'johndoe@gmail.com',
                role : 'moderator'
            };
            listInstance.addModerator('johndoe@gmail.com').then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/members');
                done();
            });
        });

    }); // .addModerator()

    describe('.removeModerator()',function() {

        it('should remove an address from the moderator role',function(done) {
            var httpMethodSpy = sinon.spy(listInstance,'delete');
            listInstance.removeModerator('caroljames@gmail.com').then(function() {
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/moderator/caroljames@gmail.com');
                expect(httpMethodSpy).to.be.calledOnce;
                done();
            });
        });

    }); // .removeModerator()

    describe('.removeOwner()',function() {

        it('should remove an address from the owner role',function(done) {
            var httpMethodSpy = sinon.spy(listInstance,'delete');
            listInstance.removeOwner('jacksmith@gmail.com').then(function() {
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/owner/jacksmith@gmail.com');
                expect(httpMethodSpy).to.be.calledOnce;
                done();
            });
        });

    }); // .removeOwner()

    describe('.rejectMessage()',function() {

        it('should reject the message',function(done) {
            var httpMethodSpy = sinon.stub(listInstance,'post',fakeRequest);
            var payload = { action : 'reject' };
            var requestId = '501dfc435f9427e3884b5ea96d64c6cbf826ef';
            listInstance.rejectMessage(requestId).then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(httpMethodSpy).to.be.calledWith(payload);
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/held/'+requestId);
                done();
            });
        });

    }); // .discardMessage()

    describe('.deferMessage()',function() {

        it('should defer the message',function(done) {
            var httpMethodSpy = sinon.stub(listInstance,'post',fakeRequest);
            var payload = { action : 'defer' };
            var requestId = '501dfc435f9427e3884b5ea96d64c6cbf826ef';
            listInstance.deferMessage(requestId).then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(httpMethodSpy).to.be.calledWith(payload);
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/held/'+requestId);
                done();
            });
        });

    }); // .deferMessage()

    describe('.discardMessage()',function() {

        it('should discard the message',function(done) {
            var httpMethodSpy = sinon.stub(listInstance,'post',fakeRequest);
            var payload = { action : 'discard' };
            var requestId = '501dfc435f9427e3884b5ea96d64c6cbf826ef';
            listInstance.discardMessage(requestId).then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(httpMethodSpy).to.be.calledWith(payload);
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/held/'+requestId);
                done();
            });
        });

    }); // .discardMessage()

    describe('.acceptMessage()',function() {

        it('should accept the message',function(done) {
            var httpMethodSpy = sinon.stub(listInstance,'post',fakeRequest);
            var payload = { action : 'accept' };
            var requestId = '501dfc435f9427e3884b5ea96d64c6cbf826ef';
            listInstance.acceptMessage(requestId).then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(httpMethodSpy).to.be.calledWith(payload);
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/held/'+requestId);
                done();
            });
        });

    }); // .acceptMessage()

    describe('.rejectRequest()',function() {

        it('should reject the request',function(done) {
            var httpMethodSpy = sinon.stub(listInstance,'post',fakeRequest);
            var payload = { action : 'reject' };
            var requestId = '501dfc435f9427e3884b5ea96d64c6cbf826ef';
            listInstance.rejectMessage(requestId).then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(httpMethodSpy).to.be.calledWith(payload);
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/held/'+requestId);
                done();
            });
        });

    }); // .rejectRequest()

    describe('.acceptRequest()',function() {

        it('should accept the request',function(done) {
            var httpMethodSpy = sinon.stub(listInstance,'post',fakeRequest);
            var payload = { action : 'accept' };
            var requestId = '501dfc435f9427e3884b5ea96d64c6cbf826ef';
            listInstance.acceptMessage(requestId).then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(httpMethodSpy).to.be.calledWith(payload);
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/held/'+requestId);
                done();
            });
        });

    }); // .acceptRequest()

    describe('.deferRequest()',function() {

        it('should defer the request',function(done) {
            var httpMethodSpy = sinon.stub(listInstance,'post',fakeRequest);
            var payload = { action : 'defer' };
            var requestId = '501dfc435f9427e3884b5ea96d64c6cbf826ef';
            listInstance.deferMessage(requestId).then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(httpMethodSpy).to.be.calledWith(payload);
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/held/'+requestId);
                done();
            });
        });

    }); // .deferRequest()

    describe('.discardRequest()',function() {

        it('should discard the request',function(done) {
            var httpMethodSpy = sinon.stub(listInstance,'post',fakeRequest);
            var payload = { action : 'discard' };
            var requestId = '501dfc435f9427e3884b5ea96d64c6cbf826ef';
            listInstance.discardMessage(requestId).then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(httpMethodSpy).to.be.calledWith(payload);
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org/held/'+requestId);
                done();
            });
        });

    }); // .discardRequest()

    describe('.getMemberByEmail()',function(done) {
        
        it('should filter list member by it\'s email address',function(done) {
            listInstance.getMemberByEmail('annefrank@gmail.com').then(function(retValue) {
                expect(retValue).to.be.instanceof(Member);
                expect(retValue._url).to.be.equal('http://localhost:8001/3.0/members/107105723399967503049225874702952776742');
                done();
            })
        });

    }); // .getMemberByEmail()

    describe('.deleteList()',function(done) {

        it('should delete the list',function(done) {
            var httpMethodSpy = sinon.spy(listInstance,'delete');
            listInstance.deleteList().then(function(retValue) {
                expect(listInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/black_perl@example1.org');
                expect(httpMethodSpy).to.be.calledOnce;
                done();
            });
        });

    }); // .deleteList()

}); // List