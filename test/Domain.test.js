'use strict';

/**
Unit tests for the Domain module
**/

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
chai.use(require('sinon-chai'));
var nock = require('nock');
var path = require('path');

var ApiRequest = require('../lib/shared/ApiRequest.js');
var BaseRequest = require('../lib/shared/BaseRequest.js');
var List = require('../lib/List.js');
var User = require('../lib/User.js');
var Domain = require('../lib/Domain.js');


// loading fixtures
var fixturesPath = path.join(__dirname,'fixtures/domain.test.fixtures.js');
try {
    require(fixturesPath);
} catch(e) {
    console.log(e);
    throw new Error('Fixtures file not found');
}

describe('Domain',function() {
    var domainInstance, responseData, options;

    options = {
        endpoint : 'http://localhost:8001/3.0',
        username : 'restadmin',
        password : 'restpass'
    };

    responseData = { 
        self_link: 'http://localhost:8001/3.0/domains/somedomain.org',
        mail_host: 'somedomain.org',
        url_host: 'somedomain.org',
        description: 'Testing domain',
        http_etag: '"b938728cd8749a240a62657cba2ee2d79819b734"',
        base_url: 'http://somedomain.org' 
    };

    beforeEach(function() {
        domainInstance = new Domain(options,
                                    'http://localhost:8001/3.0/domains/somedomain.org');
    });

    describe('constructor',function() {

        it('should be an instance of BaseRequest',function() {
            expect(domainInstance).to.be.instanceof(BaseRequest);
        });

        it('should be an instance of ApiRequest',function() {
            expect(domainInstance).to.be.instanceof(ApiRequest);
        });

        it('should be an instance of Domain',function() {
            expect(domainInstance).to.be.instanceof(Domain);
        });

        it('should initialise the instance properties',function() {
            expect(domainInstance._supportedMethods).to.be.deep.equal(['get','post','delete']);
            expect(domainInstance._rootTemplate).to.be.equal('/domains/somedomain.org');
            expect(domainInstance._url).to.be.equal('http://localhost:8001/3.0/domains/somedomain.org');
        });

    }); // constructor

    describe('.getInfo()',function(done) {

        it('._renderURI() should return the proper URL',function(done) {
            domainInstance.getInfo().then(function() {
                expect(domainInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/domains/somedomain.org');
                done();
            });
        });

        it('should call the callback function with the Domain instance',function(done) {
            var callback = function(err,data) {

            };
            var callbackSpy = sinon.spy(callback);
            domainInstance.getInfo(callbackSpy).then(function() {
                expect(callbackSpy).to.be.calledOnce;
                expect(callbackSpy).to.be.calledWith(null,domainInstance);
                expect(domainInstance._info).to.be.deep.equal(responseData);
                done();
            });
        });

        it('should provide a promise to the Domain instance',function(done) {
            domainInstance.getInfo().then(function(retValue) {
                expect(retValue).to.be.deep.equal(domainInstance);
                expect(domainInstance._info).to.be.deep.equal(responseData);
                done();
            });
        });  

    }); // .getInfo()

    describe('._setAccessors()',function(done) {

        it('should set the accessors on the Domain instance',function(done) {
            domainInstance.getInfo().then(function() {
                var keys = ['self_link','mail_host','url_host','description','base_url'];
                keys.forEach(function(key) {
                    expect(domainInstance[key]).to.be.equal(responseData[key]);
                });
                done();
            });
        });

    }); // ._setAccessors()

    describe('.getOwners()',function(done) {

        it('should provide promise to the domain onwers',function(done) {
            var httpMethodSpy = sinon.spy(domainInstance,'get');
            domainInstance.getOwners().then(function(retValue) {
                expect(domainInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/domains/somedomain.org/owners');
                expect(httpMethodSpy).to.be.calledOnce;
                retValue.forEach(function(entry) {
                    expect(entry).to.be.instanceof(User);
                });
                done();
            });
        });
    }); // .getOwners()

    describe('.getLists()',function(done) {

        it('should provide a promise to the lists corresponding to the domain',function(done) {
            var httpMethodSpy = sinon.spy(domainInstance,'get');
            domainInstance.getLists().then(function(retValue) {
                expect(httpMethodSpy).to.be.calledTwice;
                expect(domainInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/domains/somedomain.org/lists');
                retValue.forEach(function(entry) {
                    expect(entry).to.be.instanceof(List);
                });
                done();
            });
        });

    });  // .getLists()

    describe('.createList()',function(done) {

        it('should create a list',function(done) {
            var httpMethodSpy = sinon.spy(domainInstance,'post');
            domainInstance.createList('foolist').then(function(retValue) {
                expect(httpMethodSpy).to.be.calledOnce;
                var payload = {
                    fqdn_listname : 'foolist' + '@' + domainInstance.mail_host
                };
                expect(httpMethodSpy).to.be.calledWith(payload,null,'identity');
                expect(domainInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists');
                expect(retValue).to.be.instanceof(List);
                expect(retValue._url).to.be.equal('http://localhost:8001/3.0/lists/foolist.somedomain.org');
                done();
            });
        });

    }); // .createList()

    describe('.removeOwners()',function(done) {

        it('should remove all the list owners',function(done) {
            var httpMethodSpy = sinon.spy(domainInstance,'delete');
            domainInstance.removeOwners().then(function(retValue) {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(domainInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/domains/somedomain.org/owners');
                expect(retValue).to.be.deep.equal(domainInstance);
                done();
            });
        });

    }); // .removeOwners()

    describe('.addOwner()',function(done) {

        it('should add a domain owner',function(done) {
            var httpMethodSpy = sinon.spy(domainInstance,'post');
            domainInstance.addOwner('anne@gmail.com').then(function(retValue) {
                expect(httpMethodSpy).to.be.calledOnce;
                var payload = { owner : 'anne@gmail.com' };
                expect(httpMethodSpy).to.be.calledWith(payload);
                expect(retValue).to.be.deep.equal(domainInstance);
                done();
            });
        });

    }); // .addOwner()

    describe('.toString()',function() {
        it('should return the string representation of the Domain instance',function() {
            expect(domainInstance.toString()).to.be.equal('<Domain somedomain.org>');
        });
    });

}); // Domain
