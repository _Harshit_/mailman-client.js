'use strict';

/**
* Unit tests for the BaseRequest module
**/

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var sandbox = require('sandboxed-module');
var BaseRequest = require('../../lib/shared/BaseRequest.js');
var customTransformer = require('../transformer');

chai.use(require('sinon-chai'));

describe('BaseRequest',function(){
    var request;

    beforeEach(function() {
        request = new BaseRequest();
    });

    describe('constructor',function(){

        it('should create a BaseRequest instance',function() {
            expect(request instanceof BaseRequest).to.be.true;
        });

        it('should set any passed in arguments as options',function(){
            request = new BaseRequest({
                'username' : 'root',
                'url' : 'http://black-perl.me'
            })
            expect(request._options.username).to.be.equal('root');
            expect(request._options.url).to.be.equal('http://black-perl.me');
        })

        it('should define a _supportedMethods property which is an array',function(){
            var supportedMethods = request._supportedMethods.join('-');
            expect(request._supportedMethods).to.be.instanceof(Array);
            expect(supportedMethods).to.be.equal('head-get-put-post-delete-patch');
        });

        it('should define a _path property',function(){
            expect(request._path).to.deep.equal({});
        });

        it('should define a _template property',function(){
            expect(request._template).to.be.equal('');
        });

        it('should define a callable _checkMethodSupport property',function(){
            expect(request._checkMethodSupport('get')).to.be.undefined;

            it('should throw an error when passed in unsupported call',function(){
                request._supportedMethods = ['get'];
                expect(request._checkMethodSupport('put')).to.throw(Error);
            });
        });

        it('should have a callable property _checkAuthParameters which return true if \
            _options.username and _options.password properties are set',function(){

            var request = new BaseRequest({username:'testuser',password:'password'});
            expect(request._options).to.have.property('username');
            expect(request._options).to.have.property('password');
            expect(request._checkAuthParameters()).to.be.true;
            expect(request._options.forceAuth).to.be.true;

            it('should return False when username and password are not passesd in while instantiating',function(){
                var request = new BaseRequest();
                expect(request._checkAuthParameters()).to.be.false;
                expect(request._options.forceAuth).to.be.false;
                expect(request._options).to.have.property('username');
                expect(request._options).to.have.property('password');
            });
        });

    });  // constructor

    describe('_auth',function() {
        var agentMock, SandboxedRequest;

        beforeEach(function(){
            // create the agentMock 
            var SuperAgentMock = require('./mocks/superagent-mock.js');
            agentMock = new SuperAgentMock();
            // Need to sandbox BaseRequest.js module with SuperAgentMock passed in
            SandboxedRequest = sandbox.require('../../lib/shared/BaseRequest.js', {
                requires: {
                    'superagent': agentMock
                },
                sourceTransformers : {
                    'customTransformer' : customTransformer
                }
            });

        });

        describe('superagent\'s auth behavior',function() {

            it('should set basic auth if username and password are supplied',function() {
                var baseRequest = new SandboxedRequest({username:'testuser',password:'password'});
                sinon.spy(agentMock,'auth');

                var retValue = baseRequest._auth(agentMock);

                expect(agentMock.auth).to.have.been.calledOnce;
                expect(agentMock.auth).to.have.been.calledWith('testuser','password');
                expect(retValue).to.be.equal(agentMock);
            });

            it('should not set basic auth if password in not supplied',function(){
                var baseRequest = new SandboxedRequest({username:'testuser'});
                sinon.spy(agentMock,'auth');

                var retValue = baseRequest._auth(agentMock);

                expect(agentMock.auth).to.have.callCount(0);
                expect(retValue).to.be.equal(agentMock);
            });

            it('should not set basic auth if username in not supplied',function(){
                var baseRequest = new SandboxedRequest({password:'password'});
                sinon.spy(agentMock,'auth');

                var retValue = baseRequest._auth(agentMock);

                expect(agentMock.auth).to.have.callCount(0);
                expect(retValue).to.be.equal(agentMock);
            });

        });

        describe('_checkAuthParameters',function() {

            it('should call _checkAuthParameters with no arguments',function(){
                var baseRequest = new SandboxedRequest({username:'testuser',password:'password'});
                sinon.spy(baseRequest,'_checkAuthParameters');

                baseRequest._auth(agentMock);

                expect(baseRequest._checkAuthParameters).to.have.been.calledOnce;
                expect(baseRequest._checkAuthParameters).to.have.been.calledWith(); 
            });
        });

    }); // _auth

    describe('HTTP request methods',function() {

        var agentMock, SandboxedRequest, baseRequest;

        beforeEach(function() {
            // create the agentMock 
            var SuperAgentMock = require('./mocks/superagent-mock.js');
            agentMock = new SuperAgentMock();
            // Need to sandbox BaseRequest.js module with SuperAgentMock passed in
            SandboxedRequest = sandbox.require('../../lib/shared/BaseRequest.js', {
                requires: {
                    'superagent': agentMock
                },
                sourceTransformers : {
                    'customTransformer' : customTransformer
                }
            });
            baseRequest = new SandboxedRequest({'endpoint':'http://black-perl.me/'});
        });

        describe('.get()',function(done) {
            
            it('should trigger a HTTP GET request',function() {
                sinon.spy(agentMock,'get');
                // as we are forcing not to send the request, end should be a stub
                sinon.stub(agentMock,'end');

                baseRequest.get();

                expect(agentMock.get).to.have.been.calledOnce;
                expect(agentMock.get).to.have.been.calledWith('http://black-perl.me/');
                expect(agentMock.end).to.have.been.calledOnce;
            });

            it('should call the passed in callback function',function(done) {
                var callback = function(err,data) {
                    // pass
                }
                var callbackSpy = sinon.spy(callback);
                sinon.spy(agentMock,'get');
                // set the fake response 
                var fakeData = { body : 'body', headers : {} };
                agentMock._response = fakeData;

                baseRequest.get(callbackSpy).then(function() {
                    expect(agentMock.get).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledWith(null,'body');
                    done();
                });
            });

            it('should return the full response when transform function is identity',function(done) {
                var callback = function(err,data) {
                    // pass
                }
                var callbackSpy = sinon.spy(callback);
                // set the fake response 
                var fakeData = { body : 'body', headers : {'status':'200 OK'} };
                agentMock._response = fakeData;

                baseRequest.get(callbackSpy,'identity').then(function() {
                    expect(callbackSpy).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledWith(null,fakeData);
                    done();
                });
            });

            it('should return a Promise to the request response',function(done) {
                agentMock._response = { body : 'body', headers : {} };
                var promise = baseRequest.get();
                expect(promise).to.have.property('then');
                expect(promise.then).to.be.a('function');
                promise.then(function(data) {
                    expect(data).to.be.equal('body');
                    done();
                });
            });
        }); // .get()

        describe('.post()',function(done) {

            it('should trigger a HTTP POST request',function() {
                sinon.spy(agentMock,'post');
                sinon.spy(agentMock,'send');
                sinon.stub(agentMock,'end');
                var postData = {data : 'somedata'};

                baseRequest.post(postData);

                expect(agentMock.post).to.be.calledOnce;
                expect(agentMock.post).to.be.calledWith('http://black-perl.me/');
                expect(agentMock.send).to.be.calledOnce;
                expect(agentMock.send).to.be.calledWith(postData);
                expect(agentMock.end).to.have.been.calledOnce;
            });

            it('should call the passed in callback function',function(done) {
                var callback = function(err,data) {
                    // pass
                }
                var callbackSpy = sinon.spy(callback);
                sinon.spy(agentMock,'post');
                var fakeData = {body : 'body', headers : {}};
                var postData = {some : 'data'};

                agentMock._response = fakeData;

                baseRequest.post(postData,callbackSpy).then(function() {
                    expect(agentMock.post).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledWith(null,'body');
                    done();
                });
            });

            it('should return the full response when transform function is identity',function(done) {
                var callback = function(err,data) {
                    // pass
                }
                var callbackSpy = sinon.spy(callback);
                // set the fake response 
                var fakeData = { body : 'body', headers : {'status':'200 OK'} };
                agentMock._response = fakeData;
                // post data
                var postData = {some : 'data'};

                baseRequest.post(postData,callbackSpy,'identity').then(function() {
                    expect(callbackSpy).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledWith(null,fakeData);
                    done();
                });
            });

            it('should return a Promise to the request response',function(done) {
                agentMock._response = { body : 'body', headers : {} };
                var postData = {some : 'data'};
                var promise = baseRequest.post(postData);
                expect(promise).to.have.property('then');
                expect(promise.then).to.be.a('function');
                promise.then(function(data) {
                    expect(data).to.be.equal('body');
                    done();
                });
            });
        }); // .post()

     describe('.head()',function(done) {

            it('should trigger a HTTP HEAD request',function() {
                sinon.spy(agentMock,'head');
                sinon.stub(agentMock,'end');

                baseRequest.head();

                expect(agentMock.head).to.be.calledOnce;
                expect(agentMock.head).to.be.calledWith('http://black-perl.me/');
                expect(agentMock.end).to.be.calledOnce;
            });

            it('should call the passed in callback function',function(done) {
                var callback = function(err,data) {
                    // pass
                }
                var callbackSpy = sinon.spy(callback);
                sinon.spy(agentMock,'head');
                var fakeData = {body : 'body', headers : {'status':'200 OK'}};

                agentMock._response = fakeData;

                baseRequest.head(callbackSpy).then(function() {
                    expect(agentMock.head).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledWith(null,'body');
                    done();
                });
            });

            it('should return the full response when transform function is identity',function(done) {
                var callback = function(err,data) {
                    // pass
                }
                var callbackSpy = sinon.spy(callback);
                // set the fake response 
                var fakeData = { body : 'body', headers : {'status':'200 OK'} };
                agentMock._response = fakeData;

                baseRequest.head(callbackSpy,'identity').then(function() {
                    expect(callbackSpy).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledWith(null,fakeData);
                    done();
                });
            });

            it('should return a Promise to the request response',function(done) {
                agentMock._response = { body : 'body', headers : {'status':'200 OK'} };
                var promise = baseRequest.head();
                expect(promise).to.have.property('then');
                expect(promise.then).to.be.a('function');
                promise.then(function(data) {
                    expect(data).to.deep.equal('body');
                    done();
                });
            });
        }); // .head()

        describe('.put()',function(done) {

            it('should trigger a HTTP PUT request',function() {
                sinon.spy(agentMock,'put');
                sinon.spy(agentMock,'send');
                sinon.stub(agentMock,'end');
                var putData = {data : 'somedata'};

                baseRequest.put(putData);

                expect(agentMock.put).to.be.calledOnce;
                expect(agentMock.put).to.be.calledWith('http://black-perl.me/');
                expect(agentMock.send).to.be.calledOnce;
                expect(agentMock.send).to.be.calledWith(putData);
            });

            it('should call the passed in callback function',function(done) {
                var callback = function(err,data) {
                    // pass
                }
                var callbackSpy = sinon.spy(callback);
                sinon.spy(agentMock,'put');
                var fakeData = {body : 'body', headers : {}};
                var putData = {some : 'data'};

                agentMock._response = fakeData;

                baseRequest.put(putData,callbackSpy).then(function() {
                    expect(agentMock.put).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledWith(null,'body');
                    done();
                });
            });

            it('should return the full response when transform function is identity',function(done) {
                var callback = function(err,data) {
                    // pass
                }
                var callbackSpy = sinon.spy(callback);
                // set the fake response 
                var fakeData = { body : 'body', headers : {'status':'200 OK'} };
                agentMock._response = fakeData;
                // post data
                var putData = {some : 'data'};

                baseRequest.put(putData,callbackSpy,'identity').then(function() {
                    expect(callbackSpy).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledWith(null,fakeData);
                    done();
                });
            });

            it('should return a Promise to the request response',function(done) {
                agentMock._response = { body : 'body', headers : {} };
                var putData = {some : 'data'};
                var promise = baseRequest.put(putData);
                expect(promise).to.have.property('then');
                expect(promise.then).to.be.a('function');
                promise.then(function(data) {
                    expect(data).to.be.equal('body');
                    done();
                });
            });
        }); // .put()

        describe('.patch()',function(done) {

            it('should trigger a HTTP PATCH request',function() {
                sinon.spy(agentMock,'patch');
                sinon.spy(agentMock,'send');
                sinon.stub(agentMock,'end');
                var patchData = {data : 'somedata'};

                baseRequest.patch(patchData);

                expect(agentMock.patch).to.be.calledOnce;
                expect(agentMock.patch).to.be.calledWith('http://black-perl.me/');
                expect(agentMock.send).to.be.calledOnce;
                expect(agentMock.send).to.be.calledWith(patchData);
            });

            it('should call the passed in callback function',function(done) {
                var callback = function(err,data) {
                    // pass
                }
                var callbackSpy = sinon.spy(callback);
                sinon.spy(agentMock,'patch');
                var fakeData = {body : 'body', headers : {}};
                var patchData = {some : 'data'};

                agentMock._response = fakeData;

                baseRequest.patch(patchData,callbackSpy).then(function() {
                    expect(agentMock.patch).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledWith(null,'body');
                    done();
                });
            });

            it('should return the full response when transform function is identity',function(done) {
                var callback = function(err,data) {
                    // pass
                }
                var callbackSpy = sinon.spy(callback);
                // set the fake response 
                var fakeData = { body : 'body', headers : {'status':'200 OK'} };
                agentMock._response = fakeData;
                // post data
                var patchData = {some : 'data'};

                baseRequest.patch(patchData,callbackSpy,'identity').then(function() {
                    expect(callbackSpy).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledWith(null,fakeData);
                    done();
                });
            });
            
            it('should return a Promise to the request response',function(done) {
                agentMock._response = { body : 'body', headers : {} };
                var patchData = {some : 'data'};
                var promise = baseRequest.patch(patchData);
                expect(promise).to.have.property('then');
                expect(promise.then).to.be.a('function');
                promise.then(function(data) {
                    expect(data).to.be.equal('body');
                    done();
                });
            });
        }); // .patch()

        describe( '.delete()', function(done) {

            it( 'should trigger an HTTP DELETE request', function() {
                sinon.spy( agentMock, 'del' );
                sinon.stub( agentMock, 'end' );

                baseRequest.delete();

                expect(agentMock.del).to.have.been.calledOnce;
                expect(agentMock.del).to.have.been.calledWith('http://black-perl.me/');
                expect(agentMock.end).to.have.been.calledOnce;
            });

            it( 'should invoke a callback, if provided', function(done) {
                var callback = function(err,data) {
                    // pass
                }
                var callbackSpy = sinon.spy(callback);
                sinon.spy(agentMock,'del');
                // set the fake response 
                var fakeData = { body : 'body', headers : {} };
                agentMock._response = fakeData;

                baseRequest.delete(callbackSpy).then(function() {
                    expect(agentMock.del).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledWith(null,'body');
                    done();
                });
            });

            it('should return the full response when transform function is identity',function(done) {
                var callback = function(err,data) {
                    // pass
                }
                var callbackSpy = sinon.spy(callback);
                // set the fake response 
                var fakeData = { body : 'body', headers : {'status':'200 OK'} };
                agentMock._response = fakeData

                baseRequest.delete(callbackSpy,'identity').then(function() {
                    expect(callbackSpy).to.have.been.calledOnce;
                    expect(callbackSpy).to.have.been.calledWith(null,fakeData);
                    done();
                });
            });

            it('should return a Promise to the request response', function(done) {
                agentMock._response = { body : 'body', headers : {} };
                var promise = baseRequest.delete();
                expect(promise).to.have.property('then');
                expect(promise.then).to.be.a('function');
                promise.then(function(data) {
                    expect(data).to.be.equal('body');
                    done();
                });
            });

        }); // .delete()

    }); // request methods

}); 